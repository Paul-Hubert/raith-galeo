package game;

import lib.gui.elements.Button;
import lib.gui.elements.Message;
import lib.gui.elements.Input;
import lib.gui.elements.Alert;
import lib.net.Client;
import lib.gui.Windu;
import lib.gui.Screen;
import lib.net.Packet;

public class GameClient {
    
    public String name;
    
    private Client serv;
    private final Windu win;
    
    String ip; int port;
    private Button pingbutton;
    
    public GameClient(Windu windu, String ip, int port) {
        win = windu;
        this.ip = ip;
        this.port = port;
    }
    
    public void start() {
        buildTitleScreen();
    }
    
    
    
    private void udpListen() {
        
        serv.setNetEvent("JoinedGame", (Packet p) -> {
            win.setAlert(new lib.gui.elements.Alert("Joined Match", () -> {}));
        });
        
        serv.setNetEvent("CreatingGame", (Packet p) -> {
            win.setAlert(new lib.gui.elements.Alert("No Matches -> Hosting", () -> {}));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                System.err.print(ex);
            }
            serv.send("Enter");
        });
        
        serv.setNetEvent("PingStart", (Packet p) -> {
            long tim = System.currentTimeMillis();
            serv.setNetEvent("PingBack", (Packet c) -> {
                serv.ping = (System.currentTimeMillis() - tim)/2;
                pingbutton.setText(String.valueOf(serv.ping));
                serv.removeNetEvent("PingBack");
            });
            serv.send("PingStart");
        });
        
    }
    
    
    private void join() {
        serv = new Client();
        win.setAlert(new Alert("Connecting...", () -> {}));
        if(serv.connect(name,ip,port)) {
            udpListen();
            win.setAlert(new Alert("Done.", () -> {}));
            pingbutton = new Button("0",90,90,()->{serv.send("Ping");});
            buildLobby();
        } else {
            win.setAlert(new Alert("Server not responding ...", () -> {join();}));
        }
    }
    
    public void exit() {
        win.close();
        if(serv!=null)serv.dispose();
        serv = null;
    }
    
    
    
    public void buildTitleScreen() {
        Screen titlescreen = new Screen("TitleScreen",win);
        
        Input username = new Input("Username",0,30,5);
        titlescreen.addGuiBox(username);
        titlescreen.addGuiBox(new Button("Join",15,50,() -> {name = username.getText(); join();}));
        titlescreen.addGuiBox(new Button("Quit",-15,50,() -> {win.setAlert(new Alert("Are you sure ?", () -> {exit();}));}));
        titlescreen.addGuiBox(new Message("Sample Text",0,-50,0));
        
        win.setScreen(titlescreen);
    }
    
    public void buildLobby() {
        Screen lobby = new Screen("Lobby", win);
        
        lobby.addGuiBox(new Button("Enter Arena", 0, -30, () -> {serv.send("Enter");buildGameUI();}));
        lobby.addGuiBox(new Button("Quit",0,20,() -> {win.setAlert(new Alert("Are you sure ?", () -> {serv.send("Exit");exit();}));}));
        pingbutton = new Button("0",90,90,()->{serv.send("Ping");});
        lobby.addGuiBox(pingbutton);
        
        win.setScreen(lobby);
    }
    
    public void buildGameUI() {
        GameScreen scr = new GameScreen(this);
        pingbutton = new Button("0",90,90,()->{serv.send("Ping");});
        scr.addGuiBox(pingbutton);
        win.setScreen(scr);
    }
    
    
    public Client getClient() {return serv;}
    public Windu getWindu() {return win;}
}
