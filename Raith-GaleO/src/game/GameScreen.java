package game;

import server.Info;
import lib.gui.Screen;
import lib.gui.elements.Alert;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.ArrayList;
import lib.net.Client;
import server.Match;
import server.Ship;
import lib.phy.Vector;
import lib.phy.World;
import lib.net.Packet;
import lib.phy.Body;
import server.Info.Input;

public class GameScreen extends Screen {
    
    private final GameClient main;
    private final Client serv;
    private final Input input;
    private Match match;
    private Ship play;
    
    public GameScreen(GameClient game) {
        super("GameScreen", game.getWindu());
        this.main = game;
        this.serv = this.main.getClient();
        input = new Info().new Input();
        udpListen();
    }
    
    
    
    private void udpListen() {
        
        serv.setNetEvent("Match", (Packet p) -> {
            match = new Match((Match) p.obj);
            match.world.balls.stream().filter((b) -> (b instanceof Ship)).map((b) -> (Ship) b).filter((s) -> (s.getID() == serv.id)).forEach((s) -> {
                play = s;
            });
            match.start();
            getAim();
            serv.send("Input",input);
        });
        
        serv.setNetEvent("Bodies", (Packet p) -> {
            match.world.balls = (ArrayList<Body>) p.obj;
            match.world.balls.stream().filter((b) -> (b instanceof Ship)).map((b) -> (Ship) b).filter((s) -> (s.getID() == serv.id)).forEach((s) -> {
                play = s;
            });
            getAim();
            serv.send("Input",input);
        });
        
        serv.setNetEvent("newPlayer", (Packet p) -> {
            match.addPlayer((int) p.obj);
        });
        serv.setNetEvent("removePlayer", (Packet p) -> {
            match.removePlayer((int) p.obj);
            if(match.players.isEmpty()) {
                match.loopOn = false;
            }
        });
        serv.setNetEvent("Input", (Packet p) -> {
            Info.Input in = (Info.Input) p.obj;
            match.players.get(p.id).l.AIM = in.AIM;
        });
        serv.setNetEvent("keyPressed", (Packet p) -> {
            match.keyPressed(p.id, (int) p.obj);
        });
        serv.setNetEvent("keyReleased", (Packet p) -> {
            match.keyReleased(p.id, (int) p.obj);
        });
        serv.setNetEvent("mouseWheelMoved", (Packet p) -> {
            match.mouseWheelMoved(p.id, (int) p.obj);
        });
        serv.setNetEvent("mousePressed", (Packet p) -> {
            match.mousePressed(p.id, (int) p.obj);
        });
        serv.setNetEvent("mouseReleased", (Packet p) -> {
            match.mouseReleased(p.id, (int) p.obj);
        });
    }
    
    
    @Override
    public void draw(Graphics2D g) {
        
        if(match != null) {
            if(play != null) match.world.cam = play.p;
            match.world.draw(g,win);
        }
        super.draw(g);
    }
    
    private void getAim() {
        input.AIM = new Vector(play.p, match.world.toWorldVector(win.mouse(), win));
    }
    
    public void backToLobby() {
        serv.send("Leave");
        main.buildLobby();
    }
    
    
    
    @Override
    public void keyPressed(KeyEvent ke) {
        serv.send("keyPressed", ke.getKeyCode());
        if(ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
            win.setAlert(new Alert("Are you sure ?", () -> {backToLobby();}));
        }
    }
    @Override
    public void keyReleased(KeyEvent ke) {serv.send("keyReleased", ke.getKeyCode());}
    @Override
    public void keyTyped(KeyEvent ke) {}
    @Override
    public void mouseWheelMoved(MouseWheelEvent mwe) {serv.send("mouseWheelMoved", mwe.getScrollAmount());}
    @Override
    public void mousePressed(MouseEvent me) {serv.send("mousePressed", me.getButton());}
    @Override
    public void mouseReleased(MouseEvent me) {serv.send("mouseReleased", me.getButton());}
}
