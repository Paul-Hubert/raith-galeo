/*
 * Copyright (C) 2016 moi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package space;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import lib.gui.Screen;
import lib.gui.Windu;
import lib.net.Server;
import lib.phy.Vector;
import static space.System.G;

/**
 *
 * @author moi
 */
public class Space extends Screen {
    
    //28458.981324 km/h
    
    public System world;
    boolean loopOn = true;
    boolean UP,DOWN,LEFT,RIGHT,LCLICK,RCLICK,SPACE;
    
    public static int SPAN_SPEED = 8, ZOOM_SPEED = 50;
    
    
    public Space(Windu windu) {
        super("Sandbox",windu);
        
        world = new System();
        
        Planet sun, earth, moon;
        
        sun = new Planet(new Vector(),50);
        sun.m = 1000000;
        earth = new Planet(new Vector(300,0),5);
        earth.m = 10000;
        earth.v.y = (float) Math.sqrt(G*sun.m/earth.p.x);
        moon = new Planet(new Vector(300,20),2);
        moon.m = 2;
        
        moon.v.x = (float) Math.sqrt(G*earth.m/moon.p.y);
        moon.v.y = (float) Math.sqrt(G*sun.m/moon.p.x);
        
        world.add(sun);
        world.add(earth);
        world.add(moon);
        
        world.cam = moon.p;
        
    }
    
    public void start() {
        win.setScreen(this);
        world.start();
        Server.SThread("World Thread", () -> {input();});
    }
    
    @Override
    public void keyPressed(KeyEvent ke) {
        if(ke.getKeyCode() == KeyEvent.VK_UP) UP = true;
        if(ke.getKeyCode() == KeyEvent.VK_DOWN) DOWN = true;
        if(ke.getKeyCode() == KeyEvent.VK_LEFT) LEFT = true;
        if(ke.getKeyCode() == KeyEvent.VK_RIGHT) RIGHT = true;
        if(ke.getKeyCode() == KeyEvent.VK_ESCAPE) this.dispose();
    }
    @Override
    public void keyReleased(KeyEvent ke) {
        if(ke.getKeyCode() == KeyEvent.VK_UP) UP = false;
        if(ke.getKeyCode() == KeyEvent.VK_DOWN) DOWN = false;
        if(ke.getKeyCode() == KeyEvent.VK_LEFT) LEFT = false;
        if(ke.getKeyCode() == KeyEvent.VK_RIGHT) RIGHT = false;
    }
    @Override
    public void keyTyped(KeyEvent ke) {}
    @Override
    public void mouseWheelMoved(MouseWheelEvent mwe) {
        world.zoom += mwe.getPreciseWheelRotation()*world.zoom/ZOOM_SPEED;
    }
    @Override
    public void mousePressed(MouseEvent me) {
        if(me.getButton() == 1) {
            LCLICK = true;
        } else if(me.getButton() == 3) {
            RCLICK = true;
        }
    }
    @Override
    public void mouseReleased(MouseEvent me) {
        if(me.getButton() == 1) {
            LCLICK = false;
        } else if(me.getButton() == 3) {
            RCLICK = false;
        }
    }
    
    private void input() {
        while(loopOn) {
            
            if(UP||DOWN||LEFT||RIGHT) {
                world.cam = new Vector(world.cam);
                if(UP) world.cam.y -= SPAN_SPEED/world.zoom;
                if(DOWN) world.cam.y += SPAN_SPEED/world.zoom;
                if(LEFT) world.cam.x -= SPAN_SPEED/world.zoom;
                if(RIGHT) world.cam.x += SPAN_SPEED/world.zoom;
            }
            
            
            
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(Space.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    @Override
    public void draw(Graphics2D g) {
        world.draw(g,win);
        super.draw(g);
    }
    
    @Override
    public void dispose() {
        world.SaveWorld("world.wld");
        world.dispose();
        super.dispose();
        java.lang.System.exit(0);
    }
}
