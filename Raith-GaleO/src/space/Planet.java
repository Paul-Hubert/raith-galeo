/*
 * Copyright (C) 2016 moi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package space;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RadialGradientPaint;
import java.awt.geom.Point2D;
import lib.gui.Windu;
import lib.phy.Ball;
import lib.phy.Vector;

/**
 *
 * @author moi
 */
public class Planet extends Ball {
    
    public Planet(Vector pp, float R) {
        super(pp, R);
        m = (long) (R * 1000);
    }
    
    @Override
    public void draw(Graphics2D g, Windu win) {
        int r2 = (int) r;
        Vector v2 = world.toWinduVector(p, win);
        
        Point2D center = new Point2D.Float(v2.x, v2.y);
        float radius = r * world.zoom;
        float[] dist = {0.7f,1.0f};
        Color[] colors = {Color.WHITE, Color.BLACK};
        
        RadialGradientPaint rgp = new RadialGradientPaint(center, radius, dist, colors);
        
        g.setPaint(rgp);
        
        g.fillOval((int) (v2.x - r2 * world.zoom), (int) (v2.y - r2 * world.zoom), (int) (r2 * 2 * world.zoom), (int) (r2 * 2 * world.zoom));
    }
}
