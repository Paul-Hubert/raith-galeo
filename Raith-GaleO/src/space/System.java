/*
 * Copyright (C) 2016 moi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package space;

import java.awt.Color;
import java.awt.Graphics2D;
import lib.gui.Windu;
import lib.phy.Body;
import lib.phy.Vector;
import lib.phy.World;

/**
 *
 * @author moi
 */
public class System extends World {
    
    public static final double G = 0.1d;
    
    @Override
    public void step(float t) {
        
        for(int i = 0; i<balls.size();i++) {
            Body a = balls.get(i);
            if(a == null) break;
            a.step(t);
            a.move(t);
            //a.v.add(World.gravity.mult(t/1000));
            //a.collide(map);
            for(int j = 0; j < walls.size(); j++) {
                a.collide(walls.get(j));
            }
        }
        
        for(int i = 0; i < balls.size(); i++) {
            Body a = balls.get(i);
            if(a == null) break;
            for(int j = i + 1; j < balls.size(); j++) {
                Body b = balls.get(j);
                double d = Body.collide(a,b);
                Vector aux = new Vector(a.p).sub(b.p);
                if(d == 0) {
                    d = aux.length();
                }
                a.v.sub(new Vector(aux).mult(G*b.m*t/(d*d*d*1000)));
                b.v.add(new Vector(aux).mult(G*a.m*t/(d*d*d*1000)));
            }
        }
    }
    
    @Override
    public void draw(Graphics2D g, Windu win) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, win.size().width, win.size().height);
        
        for(int i = 0; i < balls.size(); i++) {
            if(balls.get(i) == null) {
                java.lang.System.out.println("windu null");
            } else balls.get(i).draw(g, win);
        }
    }
}
