package twist;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import lib.gui.Screen;
import lib.gui.Windu;
import lib.phy.World;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import lib.gui.elements.Message;
import lib.net.Server;
import lib.phy.Ball;
import lib.phy.Vector;
import lib.phy.Wall;

public class Game extends Screen {
    
    public World world;
    Random ran = new Random();
    boolean loopOn = true;
    boolean UP,DOWN,LEFT,RIGHT,LCLICK,RCLICK,SPACE;
    
    private Wall e;
    Message message;
    
    public Game(Windu windu) {
        super("Sandbox",windu);
        
        world = new World();
        e = new Wall(world);
        world.walls.add(e);
        
        //world.addAction((World w)->{w.balls.add(new Ball(new Vector(0f,0f),40));});
        //world.addAction((World w)->{w.balls.add(new Ball(new Vector(0f,200f),50));});
        
        world.SaveWorld("world.wld");
        World.LoadWorld("world.wld");
        
    }
    
    public void start() {
        message = new Message("0",90,90,0);
        this.addGuiBox(message);
        
        win.setScreen(this);
        world.start();
        Server.SThread("World Thread", () -> {input();});
    }
    
    @Override
    public void keyPressed(KeyEvent ke) {
        if(ke.getKeyCode() == KeyEvent.VK_UP) UP = true;
        if(ke.getKeyCode() == KeyEvent.VK_DOWN) DOWN = true;
        if(ke.getKeyCode() == KeyEvent.VK_LEFT) LEFT = true;
        if(ke.getKeyCode() == KeyEvent.VK_RIGHT) RIGHT = true;
        if(ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
            this.dispose();
        } if(ke.getKeyCode() == KeyEvent.VK_N) {
            e = new Wall(world);
            world.walls.add(e);
        }
    }
    @Override
    public void keyReleased(KeyEvent ke) {
        if(ke.getKeyCode() == KeyEvent.VK_UP) UP = false;
        if(ke.getKeyCode() == KeyEvent.VK_DOWN) DOWN = false;
        if(ke.getKeyCode() == KeyEvent.VK_LEFT) LEFT = false;
        if(ke.getKeyCode() == KeyEvent.VK_RIGHT) RIGHT = false;
    }
    @Override
    public void keyTyped(KeyEvent ke) {}
    @Override
    public void mouseWheelMoved(MouseWheelEvent mwe) {}
    @Override
    public void mousePressed(MouseEvent me) {
        if(me.getButton() == 1) {
            LCLICK = true;
            Ball ball;
            if(RIGHT) {
                ball = new Ball(world.toWorldVector(win.mouse(),win), 10);
            } else if(UP) {
                ball = new Ball(world.toWorldVector(win.mouse(),win), 100);
            } else if(LEFT) {
                int r = (int) (5+ran.nextFloat()*2);
                ball = new Ball(world.toWorldVector(win.mouse(),win), r);
            } else {
                int r = (int) (1);
                ball = new Ball(world.toWorldVector(win.mouse(),win), r);
            }
            world.addAction((World w)->{w.add(ball);});
        } else if(me.getButton() == 3) {
            RCLICK = true;
            Vector p = world.toWorldVector(win.mouse(),win);
            e.addPoint((int) (p.x), (int) (p.y));
        }
    }
    @Override
    public void mouseReleased(MouseEvent me) {
        if(me.getButton() == 1) {
            LCLICK = false;
        } else if(me.getButton() == 3) {
            RCLICK = false;
        }
    }
    
    private void input() {
        while(loopOn) {
            
            if(LCLICK) {
                Ball ball;
                if(LEFT) {
                    int r = (int) (5+ran.nextFloat()*2);
                    ball = new Ball(world.toWorldVector(win.mouse(),win), r);
                } else {
                    int r = (int) (1);
                    ball = new Ball(world.toWorldVector(win.mouse(),win), r);
                }
                world.addAction((World w)->{w.add(ball);});
            }
            
            if(DOWN) {
                World.gravity = new Vector(100-World.gravity.x, 100-World.gravity.y);
                DOWN = false;
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    @Override
    public void draw(Graphics2D g) {
        world.draw(g,win);
        super.draw(g);
    }
    
    @Override
    public void dispose() {
        world.SaveWorld("world.wld");
        world.dispose();
        super.dispose();
        System.exit(0);
    }
}
