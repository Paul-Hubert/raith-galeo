package main;

import game.GameClient;
import lib.gui.Screen;
import lib.gui.Windu;
import lib.gui.elements.Alert;
import lib.gui.elements.Button;
import lib.gui.elements.Message;
import server.HubServer;
import lib.net.Server;
import space.Space;
import twist.Game;

public class Main {
    public static String ip = "88.120.21.5";
    public static int port = 25565;
    
    public static final Windu win = new Windu("Raith-GaleO");
    
    public static void main(String[] args) {
        
        Screen scr = new Screen("TitleScreen",win);
        
        scr.addGuiBox(new Button("Multiplayer", 0,-25,() -> {startClient();}));
        scr.addGuiBox(new Button("SandBox", 0,0,() -> {startSand();}));
        scr.addGuiBox(new Button("Solar System", 0,25,() -> {startSpace();}));
        scr.addGuiBox(new Button("Quit",0,50,() -> {win.setAlert(new Alert("Are you sure ?", () -> {System.exit(0);}));}));
        scr.addGuiBox(new Message("Sample Text",0,-50,0));
        
        win.setScreen(scr);
        
        win.open();
        
    }
    
    
    public static void startClient() {
        GameClient game = new GameClient(win, ip, port);
        game.start();
    }
    public static void startSand() {
        Game game = new Game(win);
        game.start();
    }
    public static void startHubServer() {
        HubServer sp = new HubServer(port);
        Server.SThread("HubServer start", () -> {sp.start();});
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {}
    }

    public static void startSpace() {
        Space game = new Space(win);
        game.start();
    }
}
