package lib.net;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.logging.Level;
import java.util.logging.Logger;
import static lib.net.Server.SThread;

public class Client {
    
    public int id = 0;
    public long ping;
    
    public boolean ready = false;
    private boolean listening = true;
    
    private DatagramSocket socket;
    private byte[] bit;
    private NetEventListener evl = new NetEventListener();
    
    public boolean connect(String name, String ip, int port) {
        
        Thread t = new Thread(() -> {
            openUDP(ip,port);
            if(!socket.isConnected() || socket.isClosed()) return;
            
            setNetEvent("ID", (Packet p) -> {
                id = (int) p.obj;
                ready = true;
            });
            SThread("Net Event Listener Thread",()->{listen();});
            send("Hello",name);
            while(!ready) {}
        });
        t.setName("Client connect");
        t.start();
        
        try {
            t.join(2000);
        } catch (InterruptedException ex) {}
        
        if(!ready) {
            System.out.println("Server timed out");
            listening = false;
            t.interrupt();
            socket.close();
            return false;
        }
        return true;
    }
    
    private void listen() {
        while(listening) {
            Packet pak = receive();
            if(pak != null) {
                evl.triggerNetEvent(pak);
            } else {
                listening = false;
            }
        }
    }
    
    public void setNetEvent(String type, Packet.NetEvent event) {
        evl.setNetEvent(type, event);
    }
    
    public void removeNetEvent(String type) {
        evl.removeNetEvent(type);
    }
    
    private void openUDP(String ip,int port) {
        try {
            bit = new byte[8192];
            InetAddress ipad = InetAddress.getByName(ip);
            socket = new DatagramSocket();
            socket.connect(ipad, port);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void send(Packet obj) {
        byte[] send = Packet.serialize(obj);
        DatagramPacket pakt = new DatagramPacket(send,send.length);
        try {
            socket.send(pakt);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void send(String type, Object obj) {
        send(new Packet(id,type,obj));
    }
    
    public void send(String type) {
        send(new Packet(id,type,null));
    }
    
    private Packet receive() {
        DatagramPacket pakt = new DatagramPacket(bit,bit.length);
        try {
            socket.receive(pakt);
        } catch (java.net.SocketException ex) {
            System.out.println("Socket closed");
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Packet.deserialize(pakt.getData());
    }
    
    public void dispose() {
        listening = false;
        socket.disconnect();
        socket.close();
    }
}
