/*
 * Copyright (C) 2015 moi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lib.net;

import java.util.HashMap;

/**
 *
 * @author moi
 */
public class NetEventListener {
    
    private HashMap<String,Packet.NetEvent> events = new HashMap<>();
    
    public void setNetEvent(String type, Packet.NetEvent event) {
        events.remove(type);
        events.put(type,event);
    }
    
    public void removeNetEvent(String type) {
        events.remove(type);
    }
    
    public void triggerNetEvent(Packet p) {
        if(events.containsKey(p.type)) {
            events.get(p.type).receive(p);
        } else {
            System.out.println(p.type);
            System.out.println("Packet missed.");
        }
    }
}
