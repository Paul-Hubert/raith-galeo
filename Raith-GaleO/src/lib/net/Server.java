package lib.net;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;

public class Server {
    
    private DatagramSocket udp;
    public HashMap<Integer,Client> con;
    private NetEventListener evl = new NetEventListener();
    boolean listening = true;
    private byte[] bit;
    
    public Server(int port) {
        try {
            con = new HashMap<>();
            udp = new DatagramSocket(port);
            bit = new byte[8192];
            SThread("Net Event Listener Thread",()->{listen();});
        } catch (java.net.BindException ex) {
            listening = false;
            System.err.println("Bind Exception");
        } catch (java.io.IOException ex) {
            java.util.logging.Logger.getLogger(Server.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    
    private void listen() {
        while(listening) {
            DatagramPacket pak = receive();
            Packet rept = Packet.deserialize(pak.getData());
            
            if(rept.type.equals("Hello")) {
                int id = createClient((String) rept.obj, pak.getAddress(), pak.getPort());
                Client dc = con.get(id);
                rept = new Packet(id,rept.type,rept.obj);
                dc.send("ID", id);
            }
            
            evl.triggerNetEvent(rept);
        }
    }
    
    private DatagramPacket receive() {
        DatagramPacket pakt = new DatagramPacket(bit,bit.length);
        try {
            udp.receive(pakt);
        } catch (java.io.IOException ex) {
            java.util.logging.Logger.getLogger(Server.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        return pakt;
    }
    
    public class Client {
        
        public final int id;
        public final String name;
        public long ping;
        
        public ClientInfo info;
        
        private final InetAddress ip;
        private final int port;
        
        public Client(int ID, String NAME, InetAddress IP, int PORT) {
            name = NAME;
            id = ID;
            ip = IP;
            port = PORT;
        }
        
        public void setInfo(ClientInfo i) {
            info = i;
        }
        
        private void send(Packet obj) {
            try {
                byte[] send = Packet.serialize(obj);
                DatagramPacket pakt = new DatagramPacket(send,send.length, ip, port);
                udp.send(pakt);
            } catch (java.io.IOException ex) {
                java.util.logging.Logger.getLogger(Server.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (java.util.ConcurrentModificationException ex) {
                System.out.println("fail");
            }
        }
        
        public void send(String type, Object obj) {
            send(new Packet(type,obj));
        }
        
        public void send(String type) {
            send(new Packet(type,null));
        }
        
        public void send(int id, String type, Object obj) {
            send(new Packet(id,type,obj));
        }
    }
    
    public interface ClientInfo  {}
    
    public int createClient(String NAME, InetAddress IP, int PORT) {
        int id = 0;
        while(checkId(id)) {
            id++;
        }
        Client c = new Client(id,NAME,IP,PORT);
        con.put(id, c);
        return id;
    }
    
    private boolean checkId(int id) {
        if (con.values().stream().anyMatch((cl) -> (cl.id == id))) {
            return true;
        }
        return false;
    }
    
    public void dispose() {
        listening = false;
        udp.disconnect();
        udp.close();
    }
    
    public void setNetEvent(String type, Packet.NetEvent event) {
        evl.setNetEvent(type, event);
    }
    
    public void removeNetEvent(String type) {
        evl.removeNetEvent(type);
    }

    public boolean ready() {
        return listening;
    }
    
    public String getIP() {
        return udp.getInetAddress().getHostName();
    }
    
    public int getPort() {
        return udp.getPort();
    }
    
    public static void SThread(String name, Runnable r) {
        Thread t = new Thread(r);
        t.setName(name);
        t.start();
    }
}
