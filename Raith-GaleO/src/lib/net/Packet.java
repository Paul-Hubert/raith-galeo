package lib.net;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Packet implements java.io.Serializable {
    public final int id;
    public final String type;
    public final Object obj;
    
    public Packet(String type, Object obj) {
        this.type = type;
        this.obj = obj;
        this.id = 0;
    }
    
    public Packet(int id, String type, Object obj) {
        this.type = type;
        this.obj = obj;
        this.id = id;
    }
    
    public interface NetEvent {
        public void receive(Packet p);
    }
    
    public static Packet deserialize(byte[] bit) {
        ByteArrayInputStream in = new ByteArrayInputStream(bit);
        ObjectInputStream is;
        try {
            is = new ObjectInputStream(in);
            return (Packet) is.readObject();
        } catch (java.io.EOFException ex) {
            System.out.println("too much information");
            System.exit(0);
        } catch(java.io.StreamCorruptedException ex) {
            
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Could not read Object--deserialize()");
        return null;
    }
    
    public static byte[] serialize(Packet obj) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = null;
        byte[] bit = null;

        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(obj);
            bit = bos.toByteArray();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Something happened--Packet.serialize()");
        } finally {
            //catch stuff dunno
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ex) {
                System.out.println("Something happened--Packet.serialize()");
            }

            try {
                bos.close();
            } catch (IOException ex) {
                System.out.println("Something happened--Packet.serialize()");
            }
        }
        return bit;
    }
}
