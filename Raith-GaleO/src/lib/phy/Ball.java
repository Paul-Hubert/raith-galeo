package lib.phy;

import lib.gui.Windu;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class Ball extends Body {
    
    public float r;
    
    public Ball(Vector pp, float R) {
        super(pp);
        setR(R);
    }
    
    public final void setR(float r) {
        this.r = r;
        this.m = (long) (r*10);
    }
    
    @Override
    public void collide(Rectangle map) {
        if (p.x - r < map.x) {
            if(v.x < 0) v.y -= v.y * World.Ef;
            v.x = Math.abs(v.x);
            v.x -= v.x * World.El;
            p.x = map.x + r;
            collision(new Vector(-r,0),null);
        } else if (p.x + r > map.x + map.width) {
            if(v.x > 0) v.y -= v.y * World.Ef;
            v.x = -Math.abs(v.x);
            v.x -= v.x * World.El;
            p.x = map.x + map.width - r;
            collision(new Vector(r,0),null);
        }
        if (p.y - r < map.y) {
            if(v.y < 0) v.x -= v.x * World.Ef;
            v.y = Math.abs(v.y);
            v.y -= v.y * World.El;
            p.y = map.y + r;
            collision(new Vector(0,-r),null);
        } else if (p.y + r > map.y + map.height) {
            if(v.y > 0) v.x -= v.x * World.Ef;
            v.y = -Math.abs(v.y);
            v.y -= v.y * World.El;
            p.y = map.y + map.height - r;
            collision(new Vector(0,r),null);
        }
    }
    
    @Override
    public void collide(Wall wall) {
        Rectangle rect = wall.getBounds();
        if(p.x-r < rect.x+rect.width
            && p.x+r > rect.x
            && p.y-r < rect.y+rect.height
            && p.y+r > rect.y) {
            for(int i = 0; i<wall.npoints;i++) {
                Vector t = (i >= wall.npoints - 1) ? new Vector(wall.xpoints[i]-wall.xpoints[0],wall.ypoints[i]-wall.ypoints[0]).div(wall.ls[i]) : new Vector(wall.xpoints[i]-wall.xpoints[i+1],wall.ypoints[i]-wall.ypoints[i+1]).div(wall.ls[i]);
                Vector n = t.tangent();
                if(wall.contains(new Vector(p).add(n.mult(r)).toPoint())) {
                    
                    double van,vat;
                    van = v.dot(n);
                    vat = v.dot(t);
                    if(van > 0) {
                        van -= van * World.El;
                        vat -= vat * World.Ef;
                    }
                    if(van < 0) {
                        van = -van;
                    }
                    v = new Vector().add(n.mult(-van)).add(t.mult(vat));
                    
                    do {
                        p.sub(n.mult(1));
                    } while(wall.contains(new Vector(p).add(n.mult(r)).toPoint()));
                    
                    collision(n.mult(r),null);
                    return;
                }
                
                if(Vector.dist2(new Vector(wall.xpoints[i],wall.ypoints[i]), p) < r*r) {
                    n = new Vector(p,new Vector(wall.xpoints[i],wall.ypoints[i])).norm();
                    t = n.tangent();
                    double van,vat;
                    van = v.dot(n);
                    vat = v.dot(t);
                    if(van > 0) {
                        van -= van * World.El;
                        vat -= vat * World.Ef;
                    }
                    if(van < 0) {
                        van = -van;
                    }
                    v = new Vector().add(n.mult(-van)).add(t.mult(vat));
                    
                    do {
                        p.sub(n.mult(1));
                    } while(wall.contains(new Vector(p).add(n.mult(r)).toPoint()));
                    
                    collision(n.mult(r),null);
                    return;
                }
            }
        }
    }
    
    
    
    @Override
    public void draw(Graphics2D g, Windu win) {
        int r2 = (int) r;
        Vector v2 = world.toWinduVector(p, win);
        
        g.drawOval((int) (v2.x - r2), (int) (v2.y - r2), r2 * 2, r2 * 2);
    }
}
