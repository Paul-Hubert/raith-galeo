package lib.phy;

import lib.gui.Windu;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.io.Serializable;

public class Body implements Serializable {
    
    public World world;
    public Vector p;
    public Vector v;
    public double a, va;
    
    public long m,mm;
    private boolean alive = true;
    
    public Body(Vector pp) {
        p = new Vector(pp);
        v = new Vector();
        a = 0;
        va = 0;
    }
    
    public final void move(float tps) {
        p.x += v.x * tps / 1000;
        p.y += v.y * tps / 1000;
    }
    
    public void collide(Rectangle map) {}
    public void collide(Wall wall) {}
    
    public static final double collide(Body a, Body b) {
        if(a instanceof Ball && b instanceof Ball) {
            return collide((Ball) a,(Ball) b);
        }
        return 0;
    }
    
    public static final double collide(Ball a, Ball b) {
        if (Vector.dist2(a.p,b.p) < (a.r + b.r) * (a.r + b.r)) {
            
            Vector n = new Vector(a.p.x - b.p.x, a.p.y - b.p.y);
            float dist = n.length();
            n = n.div(dist);
            
            
            Vector t = n.tangent();
            double van,vat,vbn,vbt;
            van = a.v.dot(n);
            vbn = b.v.dot(n);
            vat = a.v.dot(t);
            vbt = b.v.dot(t);
            double va,vb;
            if(van > 0) {
                van -= van * World.El;
                vat -= vat * World.Ef;
            }
            if(vbn > 0) {
                vbn -= vat * World.El;
                vbt -= vbt * World.Ef;
            }
            va = (van * (a.m - b.m) + 2 * b.m * vbn) / (a.m + b.m);
            vb = (vbn * (b.m - a.m) + 2 * a.m * van) / (a.m + b.m);
            
            
            Vector av = new Vector(),bv = new Vector();
            av.add(n.mult(va)).add(t.mult(vat));
            bv.add(n.mult(vb)).add(t.mult(vbt));
            
            a.v = av;
            b.v = bv;
            
            Vector nd = n.mult((a.r + b.r - dist) / 2);
            a.p.add(nd);
            b.p.sub(nd);
            
            a.collision(n.mult(-a.r),b);
            b.collision(n.mult(b.r),a);
            return dist;
        }
        return 0;
    }
    
    public final void kill() {
        alive = false;
    }
    
    public final boolean isAlive() {
        return alive;
    }
    
    public void step(float tps) {}
    public void collision(Vector p2, Body b) {}
    public void draw(Graphics2D g, Windu win) {}
}
