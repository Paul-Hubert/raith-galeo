package lib.phy;

import lib.gui.Windu;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import lib.net.Server;

public class World implements java.io.Serializable,Cloneable {
    
    public ArrayList<Body> balls;
    public ArrayList<Wall> walls;
    public final transient ArrayList<WorldAction> actions;
    
    public final Rectangle map;
    public boolean loopOn = true;
    
    public Vector cam = new Vector();
    public float zoom = 1;
    public static Vector gravity = new Vector(0,0);
    public static float El = 0.3f,Ef = 0.01f;
    
    public World() {
        map = new Rectangle(-600,-300,1200,600);
        walls = new ArrayList<>();
        balls = new ArrayList<>();
        actions = new ArrayList<>();
    }
    
    public World World() {
        World t = new World();
        t.walls = new ArrayList<>(walls);
        t.balls = new ArrayList<>(balls);
        return t;
    }
    
    public void start() {
        Server.SThread("World Thread", () -> {run();});
    }
    
    private void run() {
        long tim = System.currentTimeMillis();
        while(loopOn) {
            long tps = System.currentTimeMillis() - tim;
            tim = System.currentTimeMillis();
            
            check();
            step(tps);
            
            try {
                if(tps < 1) Thread.sleep(1);
            } catch (InterruptedException ex) {
                System.err.print(ex);
            }
        }
    }
    
    private void check() {
        synchronized(actions) {
            if(!actions.isEmpty()) {
                for(WorldAction act : actions) {
                    act.action(this);
                }
                actions.clear();
            }
        }
        
        for(int i = 0; i<balls.size();i++) {
            Body a = balls.get(i);
            if(a == null) {
                this.addAction((World world) -> {world.balls.remove(a);});
                System.out.println("nulled");
                break;
            } else if(!a.isAlive()) {
                this.addAction((World world) -> {world.balls.remove(a);});
            }
        }
    }
    
    public void step(float t) {
        
        for(int i = 0; i<balls.size();i++) {
            Body a = balls.get(i);
            if(a == null) break;
            a.step(t);
            a.move(t);
            a.v.add(World.gravity.mult(t/1000));
            a.collide(map);
            for(int j = 0; j < walls.size(); j++) {
                a.collide(walls.get(j));
            }
        }
        
        for(int i = 0; i < balls.size(); i++) {
            Body a = balls.get(i);
            if(a == null) break;
            for(int j = i + 1; j < balls.size(); j++) {
                Body b = balls.get(j);
                Body.collide(a,b);
            }
        }
    }
    
    public void add(Body body) {
        body.world = this;
        balls.add(body);
    }
    
    public void draw(Graphics2D g, Windu win) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, win.size().width, win.size().height);
        g.setColor(Color.WHITE);
        Vector m = toWinduVector(new Vector(map.x,map.y),win);
        g.fillRect((int) (m.x), (int) (m.y), map.width, map.height);
        for(int i = 0; i < walls.size(); i++) {
            walls.get(i).draw(g, win);
        }
        g.setColor(Color.BLACK);
        for(int i = 0; i < balls.size(); i++) {
            if(balls.get(i) == null) {
                System.out.println("windu null");
            } else balls.get(i).draw(g, win);
        }
    }
    
    public interface WorldAction {
        public void action(World world);
    }
    
    public void addAction(WorldAction act) {
        synchronized(actions) {
            actions.add(act);
        }
    }
    
    public void dispose() {
        loopOn = false;
    }
    
    public Vector toWorldVector(Vector v, Windu win) {
        Vector v2 = new Vector();
        v2.x = -win.size().width/2 + v.x + cam.x;
        v2.y = -win.size().height/2 + v.y + cam.y;
        return v2;
    }
    public Vector toWinduVector(Vector v, Windu win) {
        Vector v2 = new Vector();
        v2.x = win.size().width/2 + zoom * (v.x - cam.x);
        v2.y = win.size().height/2 + zoom * (v.y - cam.y);
        return v2;
    }
    
    public static World LoadWorld(String mapLocation) {
        File worldInfo = new File(mapLocation);
        FileInputStream file;
        ObjectInputStream obj;
        try {
            file = new FileInputStream(worldInfo);
            obj = new ObjectInputStream(file);
            World world = (World) obj.readObject();
            return world.World();
        } catch (IOException ex) {
            System.out.println("Can't find " + mapLocation);
            System.out.println(worldInfo.exists());
            Logger.getLogger(World.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ClassNotFoundException ex) {
            System.out.println("Class World not found");
            return null;
        }
    }
    
    public void SaveWorld(String mapLocation) {
        System.out.println(this.balls.size() + " particules");
        File worldInfo = new File(mapLocation);
        if(worldInfo.exists()) {
            worldInfo.delete();
        }
        
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = null;
        FileOutputStream file = null;
        
        try {
            file = new FileOutputStream(worldInfo);
            out = new ObjectOutputStream(bos);
            out.writeObject(this);
            System.out.println(bos.toByteArray().length);
            bos.writeTo(file);
        } catch (IOException ex) {
            System.out.println("Something happened--serialize()");
            Logger.getLogger(World.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                
            } catch (IOException ex) {
                System.out.println("Something happened--serialize()");
                Logger.getLogger(World.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                bos.close();
            } catch (IOException ex) {
                System.out.println("Something happened--serialize()");
                Logger.getLogger(World.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if (file != null) {
                try {
                    file.close();
                } catch (IOException ex) {
                    System.out.println("Something happened--serialize()");
                    Logger.getLogger(World.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
