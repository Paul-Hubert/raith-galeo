package lib.phy;

import lib.gui.Windu;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Vector implements java.io.Serializable,Cloneable {
    
    public float x, y;
    
    public Vector() {
        x = 0f;
        y = 0f;
    }
    
    public Vector(Vector v) {
        x = v.x;
        y = v.y;
    }
    
    public Vector(Vector a, Vector b) {
        x = b.x - a.x;
        y = b.y - a.y;
    }

    public Vector(float Vx, float Vy) {
        x = Vx;
        y = Vy;
    }
    
    public float length() {
        return (float) Math.sqrt(x*x+y*y);
    }
    
    public float len2() {
        return (float) x*x+y*y;
    }
    
    public float angle() {
        return (float) (Math.atan2(y, x));
    }
    
    public static float dist(Vector a, Vector b) {
        return (float) Math.sqrt(Vector.dist2(a,b));
    }
    
    public static float dist2(Vector a, Vector b) {
        return (a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y);
    }
    
    public Vector tangent() {
        return new Vector(-y,x);
    }
    
    public double dot(Vector b){
        double sum = x * b.x + y * b.y;
        return sum;
    }
    
    public Vector norm() {
        float nl = length();
        return new Vector(x/nl,y/nl);
    }
    
    public Vector add(Vector b) {
        x += b.x;
        y += b.y;
        return this;
    }
    
    public Vector sub(Vector b) {
        x -= b.x;
        y -= b.y;
        return this;
    }
    
    public Vector mult(double b) {
        try {
            Vector v = (Vector) this.clone();
            v.x *= b;
            v.y *= b;
            return v;
            
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Vector.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public Vector div(double b) {
        try {
            Vector v = (Vector) this.clone();
            v.x /= b;
            v.y /= b;
            return v;
            
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Vector.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }
    
    public void draw(Graphics2D g, Windu win, Vector p) {
        g.setColor(Color.red);
        g.drawLine((int) p.x,(int) p.y,(int) new Vector(p).add(this).x,(int) new Vector(p).add(this).y);
    }
    

    public Point toPoint() {
        return new Point((int)x,(int)y);
    }
}
