package lib.phy;

import lib.gui.Windu;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;

public class Wall extends Polygon {
    
    private World world;
    
    float[] ls;
    
    public Wall(World world) {
        super();
        this.world = world;
        ls = new float[npoints];
    }
    
    public void draw(Graphics2D g, Windu win) {
        g.setColor(Color.BLACK);
        
        int[] x = new int[xpoints.length];
        int[] y = new int[ypoints.length];
        
        for(int i = 0; i<npoints;i++) {
            x[i] = (int) (xpoints[i] + win.size().width/2 - world.cam.x);
            y[i] = (int) (ypoints[i] + win.size().height/2 - world.cam.y);
        }
        
        Polygon d = new Polygon(x,y,npoints);
        
        g.draw(d);
    }
    
    @Override
    public void addPoint(int x, int y) {
        super.addPoint(x, y);
        ls = new float[npoints];
        for(int i = 0; i < npoints; i++) {
            ls[i] = (i >= npoints - 1) ? new Vector(xpoints[i]-xpoints[0],ypoints[i]-ypoints[0]).length() : new Vector(xpoints[i]-xpoints[i+1],ypoints[i]-ypoints[i+1]).length();
        }
    }
}
