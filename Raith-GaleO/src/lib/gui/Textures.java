package lib.gui;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;

public class Textures {
    private HashMap<String,BufferedImage> map;
    
    public Textures() {
        map = new HashMap<>();
    }
    
    public void addIMG(String name, String filename) {
        File file = new File(filename);
        BufferedImage img = null;
        
        try {
            img = ImageIO.read(file);
        } catch (IOException ex) {
            Logger.getLogger(Textures.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        map.put(name, img);
        
    }
    
    public void initTextures(String directoryName) {
        String dir = System.getProperty("user.dir");
        
        File directory = new File(dir + directoryName);
        
        mapDir(directory);
        
    }
    
    private void mapDir(File directory) {
        
        File[] fileList = directory.listFiles();
        
        for(File fil : fileList) {
            String[] lol = fil.getName().split(Pattern.quote("."));
            //System.out.println(lol[0]);
            
            if(fil.isFile()) this.addIMG(lol[0], fil.getPath());
            else if(fil.isDirectory()) {
                mapDir(fil);
            }
        }
    }
    
    public BufferedImage get(String name) {
        return map.get(name);
    }
    
    public HashMap<String,BufferedImage> getMap() {
        return map;
    }
}
