package lib.gui.elements;

import lib.gui.Windu;
import java.awt.Color;
import java.awt.Graphics2D;

public class Alert extends GuiBox {
    
    Runnable run;
    
    public Alert(String txt, Runnable run) {
        super(txt, 0, 0);
        this.run = run;
    }
    
    public void AlertEvent() {
        Thread t = new Thread(run);
        t.setName(text + " ButtonEvent thread");
        t.start();
    }
    
    @Override
    public void drawColor(Graphics2D g, Windu win) {
        g.setColor(Color.BLACK);
    }
}
