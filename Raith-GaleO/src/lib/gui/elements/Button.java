package lib.gui.elements;

import lib.gui.Windu;
import java.awt.Color;
import java.awt.Graphics2D;


public class Button extends GuiBox {
    
    Runnable run;
    
    public Button(String txt, int x, int y, Runnable run) {
        super(txt, x, y);
        this.run = run;
    }
    
    public void ButtonEvent() {
        Thread t = new Thread(run);
        t.setName(text + " ButtonEvent thread");
        t.start();
    }
    
    @Override
    public void fillColor(Graphics2D g, Windu win) {
        if(MouseOnBox(win)) {
            g.setColor(Color.DARK_GRAY);
        }else {
            g.setColor(Color.LIGHT_GRAY);
        }
    }
    
    @Override
    public void clickEvent(int i, Windu win) {
        ButtonEvent();
        
        
    }
}
