package lib.gui.elements;

import lib.gui.Windu;
import java.awt.Color;
import java.awt.Graphics2D;

public class Message extends GuiBox {

    public Message(String txt, int x, int y, int l) {
        super(txt, x, y);
    }
    
    
    @Override
    public void fillColor(Graphics2D g, Windu win) {
        g.setColor(Color.WHITE);
    }
    @Override
    public void drawColor(Graphics2D g, Windu win) {
        g.setColor(Color.WHITE);
    }
    @Override
    public void textColor(Graphics2D g, Windu win) {
        g.setColor(Color.BLACK);
    } 
}
