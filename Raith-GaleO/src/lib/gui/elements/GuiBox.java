package lib.gui.elements;

import lib.gui.Windu;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;

public class GuiBox {
    
    String text;
    float x,y,l;
    int w,h;

    
    Rectangle Brect;
    
    public GuiBox(String txt, int x, int y) {
        this.text = txt;
        this.x = x;
        this.y = y;
        this.l = 0;
    }
    
    public void prepare(Dimension s) {
        x = x * s.width / 2 / 100;
        y = y * s.height / 2 / 100;
    }
    
    public void resize(Dimension s) {
        if(l > w) Brect = new Rectangle((int) (x + (s.width - l) / 2), (int) (y + (s.height - h) / 2) + 15, (int) l, h);
        else Brect = new Rectangle((int) (x + (s.width - w) / 2) - 8, (int) (y + (s.height - h) / 2) + 15, w + 16, h);
    }
    
    public boolean MouseOnBox(Windu win) {
        return Brect.contains(new Point((int) win.mouse().x, (int) win.mouse().y));
    }
    
    public void draw(Graphics2D g, Windu win) {
        
        FontMetrics fm = g.getFontMetrics();
        java.awt.geom.Rectangle2D str = fm.getStringBounds(text, g);
        w = (int) str.getWidth();
        h = (int) str.getHeight();
        
        int TextX = (int) (x + (win.size().width - w) / 2);
        int TextY = (int) (y + (win.size().height + h) / 2);
        
        resize(win.size());
        
        fillColor(g,win);
        g.fillRoundRect(Brect.x, Brect.y, Brect.width, Brect.height, 8, 8);
        drawColor(g,win);
        g.drawRoundRect(Brect.x, Brect.y, Brect.width, Brect.height, 8, 8);
        textColor(g,win);
        g.drawString(text, TextX, TextY);
    }
    
    public void fillColor(Graphics2D g, Windu win) {
        g.setColor(Color.LIGHT_GRAY);
    }
    
    public void drawColor(Graphics2D g, Windu win) {
        g.setColor(Color.BLACK);
    }
    
    public void textColor(Graphics2D g, Windu win) {
        g.setColor(Color.BLACK);
    }
    
    public void clickEvent(int i, Windu win) {
        
        
        
    }
    
    
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
}
