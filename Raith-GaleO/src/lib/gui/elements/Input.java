package lib.gui.elements;

import lib.gui.Windu;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;

public class Input extends GuiBox {
    
    public Input(String txt, int x, int y, int l) {
        super(txt, x, y);
        l = 5;
    }
    
    boolean focused = false;
    
    public void gainFocus() {
        focused = true;
        text = "";
    } public void loseFocus() {
        focused = false;
    }
    
    @Override
    public void fillColor(Graphics2D g, Windu win) {
        if(focused) g.setColor(Color.DARK_GRAY);
        else g.setColor(Color.LIGHT_GRAY);
    }
    
    public void keyType(KeyEvent ke) {
        if((int) (ke.getKeyChar()) != 8) text += ke.getKeyChar();
    }
    
    public void backspace() {
        try {
            text = text.substring(0, text.length() -1);
        } catch(java.lang.StringIndexOutOfBoundsException e) {}
    }
    
    @Override
    public void clickEvent(int i, Windu win) {
        win.setFocused(this);
    }
}
