package lib.gui;

import lib.gui.elements.Alert;
import lib.gui.elements.Input;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferStrategy;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JPanel;
import lib.net.Server;
import lib.phy.Vector;

public class Windu implements Listening {
    
    private final JFrame frame;
    private final BufferStrategy strat;
    private final Textures map;
    private boolean drawable;
    private Screen current;
    private Alert alert;
    private Input focusedInput;
    private final Font font;
    private final Listener l;
    
    public Windu(String str) {
        frame = new JFrame(str);
        frame.setPreferredSize(new Dimension(1024, 768));
        frame.setMinimumSize(new Dimension(1024, 768));
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setUndecorated(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(true);
        JPanel panel = new JPanel();
        frame.setContentPane(panel);
        frame.pack();
        frame.createBufferStrategy(2);
        strat = frame.getBufferStrategy();
        
        frame.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent evt) {
                resize(evt);
            }
        });
        
        map = new Textures();
        //map.initTextures("/textures");
        
        font = new Font("Arial Black", 0, 50);
        
        l = new Listener();
        frame.addKeyListener(l);
        frame.addMouseListener(l);
        frame.addMouseWheelListener(l);
        l.setListener(this);
    }
    
    
    
    private void draw() {
        if(drawable) {
            Graphics2D g = (Graphics2D) strat.getDrawGraphics();
            
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, size().width, size().height);
            
            current.draw(g);
            
            if(alert != null) alert.draw(g, this);
            
            g.dispose();
            strat.show();
        }
    }
    
    public void drawThread() {
        while(drawable) {
            draw();
            try {
                Thread.sleep(20);
            } catch (InterruptedException ex) {
                Logger.getLogger(Windu.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void open() {
        frame.setVisible(true);
        Server.SThread("Windu Thread", () -> {drawThread();});
        drawable = true;
    }
    
    public void close() {
        drawable = false;
        frame.setVisible(false);
        frame.dispose();
        
    }
    
    // <editor-fold defaultstate="collapsed" desc="Getters and Setters">
    public void setScreen(Screen ne) {
        if(current!=null) current.dispose();
        current = ne;
    }
    public Screen getScreen() {return current;}
    public Listener getListener() {return l;}
    public Textures getTextures() {return map;}
    public JFrame getFrame() {return frame;}
    public BufferStrategy getStrategy() {return strat;}
    public Boolean isOpen() {return drawable;}
    public Dimension size() {return frame.getSize();}
    public Vector mouse() {
        Point mouse = frame.getMousePosition();
        if (mouse != null) return new Vector(mouse.x,mouse.y);
        return new Vector(3, 25);
    }public Alert getAlert() {return alert;}
    public void setAlert(Alert alert) {this.alert = alert;}
    public Input getFocused() {return focusedInput;}
    public void setFocused(Input focused) {
        if(focusedInput != null) this.focusedInput.loseFocus();
        this.focusedInput = focused;
        this.focusedInput.gainFocus();
    }
    public void deFocusInput() {
        if(focusedInput != null)focusedInput.loseFocus();
        focusedInput = null;
    }
    public Font getFont() {return font;}
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Events">
    @Override
    public void keyPressed(KeyEvent ke) {
        if(ke.getKeyCode() == KeyEvent.VK_ENTER) {
            deFocusInput();
        } if(ke.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            if(focusedInput != null) {
                focusedInput.backspace();
            }
        } if(ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
            deFocusInput();
        }
        if(alert == null) {
            current.keyPressed(ke);
        }
    }
    @Override
    public void keyReleased(KeyEvent ke) {
        if(alert == null) {
            current.keyReleased(ke);
        }
    }
    @Override
    public void keyTyped(KeyEvent ke) {
        if(focusedInput != null) {
            focusedInput.keyType(ke);
        } if(alert == null) {
            current.keyTyped(ke);
        }
    }
    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if(alert == null) {
            current.mouseWheelMoved(e);
        }
    }
    @Override
    public void mousePressed(MouseEvent e) {
        if(alert == null) {
            current.mousePressed(e);
        }
    }
    @Override
    public void mouseReleased(MouseEvent e) {
        if(alert == null) {
            current.mouseReleased(e);
        }
    }
    @Override
    public void mouseClicked(MouseEvent e) {
        if(alert != null) {
            if(alert.MouseOnBox(this)) alert.AlertEvent();
            alert = null;
        } else current.mouseClicked(e);
        if(focusedInput != null) {
            if(!focusedInput.MouseOnBox(this)) deFocusInput();
        }
    }
    private void resize(ComponentEvent evt) {}
    // </editor-fold>
    
}
