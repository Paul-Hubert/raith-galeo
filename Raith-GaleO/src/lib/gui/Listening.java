package lib.gui;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

public interface Listening {
    
    public abstract void keyPressed(KeyEvent ke);
    public abstract void keyReleased(KeyEvent ke);
    public abstract void keyTyped(KeyEvent ke);
    public abstract void mouseWheelMoved(MouseWheelEvent e);
    public abstract void mousePressed(MouseEvent e);
    public abstract void mouseReleased(MouseEvent e);
    public abstract void mouseClicked(MouseEvent e);
    
}
