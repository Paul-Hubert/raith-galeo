package lib.gui;

import java.awt.event.*;

public class Listener implements KeyListener, MouseWheelListener, MouseListener, java.io.Serializable {
    
    private transient Listening l;
    
    @Override
    public void keyPressed(KeyEvent ke) {
        l.keyPressed(ke);
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        l.keyReleased(ke);
    }
    
    @Override
    public void keyTyped(KeyEvent ke) {
        l.keyTyped(ke);
    }
    
    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        l.mouseWheelMoved(e);
    }
    
    
    
    @Override
    public void mousePressed(MouseEvent e) {
        l.mousePressed(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        l.mouseReleased(e);
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
        l.mouseClicked(e);
    }
    @Override
    public void mouseEntered(MouseEvent e) {}
    @Override
    public void mouseExited(MouseEvent e) {}
    
    
    
    public void setListener(Listening l) {
        this.l = l;
    }
}
