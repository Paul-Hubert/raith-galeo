package lib.gui;

import lib.gui.elements.GuiBox;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.ArrayList;

public class Screen implements Listening {
    
    final String name;
    public Windu win;
    
    ArrayList<GuiBox> gui = new ArrayList<>();
    
    
    public Screen(String name, Windu win) {
        this.name = name;
        this.win = win;
    }
    
    public void addGuiBox(GuiBox m) {
        m.prepare(win.size());
        gui.add(m);
    }
    
    public void draw(Graphics2D g) {
        g.setFont(win.getFont());
        for(GuiBox i : gui) {
            i.draw(g,win);
        }
    }
    
    public void dispose() {
        gui.clear();
    }
    
    @Override
    public final void mouseClicked(MouseEvent e) {
        for(GuiBox i : gui) {
            if(i.MouseOnBox(win)) {
                i.clickEvent(e.getButton(), win);
            }
        }
    }
    
    @Override
    public void keyPressed(KeyEvent ke) {}
    
    @Override
    public void keyReleased(KeyEvent ke) {}
    
    @Override
    public void keyTyped(KeyEvent ke) {}
    
    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {}
    
    @Override
    public void mousePressed(MouseEvent e) {}
    
    @Override
    public void mouseReleased(MouseEvent e) {}
    
}
