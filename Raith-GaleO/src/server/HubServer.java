package server;

import java.util.HashMap;
import lib.net.Packet;
import lib.net.Server;

public class HubServer {
    
    private Server serv;
    
    private HashMap<Integer, ServerMatch> matches = new HashMap<>();
    
    public HubServer(int port) {
        serv = new Server(port);
    }
    
    public void start() {
        serv.setNetEvent("Hello", (Packet p) -> {
            serv.con.get(p.id).info = Info.newPlayerInfo();
        });
        
        serv.setNetEvent("Exit", (Packet p) -> {
            serv.con.remove(p.id);
        });
        
        serv.setNetEvent("Enter", (Packet p) -> {
            int newM = -1;
            
            for(ServerMatch m : matches.values()) {
                if(newM == -1) {
                    newM = m.id;
                }
                if(matches.get(m.id).getSize() > matches.get(newM).getSize() && matches.get(m.id).getSize() < Match.PLAYER_LIMIT) {
                    newM = m.id;
                }
            }
            
            if(newM == -1) {
                serv.con.get(p.id).send("CreatingGame");
                createMatch();
            } else {
                getPlayerInfo(p.id).setInGame(newM);
                matches.get(newM).addPlayer(p.id);
                serv.con.get(p.id).send("JoinedGame",newM);
            }
        });
        
        serv.setNetEvent("Ping", (Packet p) -> {
            serv.con.get(p.id).send("PingStart");
        });
        
        serv.setNetEvent("PingStart", (Packet p) -> {
            serv.con.get(p.id).send("PingBack");
        });
        
        serv.setNetEvent("Leave", (Packet p) -> {
            matches.get(getPlayerInfo(p.id).currentGame).removePlayer(p.id);
            getPlayerInfo(p.id).setToLobby();
        });
        
        serv.setNetEvent("Info", (Packet p) -> {
            serv.con.get(p.id).info = (Server.ClientInfo) p.obj;
        });
    }
    
    public Info.Client getPlayerInfo(int id) {
        return (Info.Client) serv.con.get(id).info;
    }
    
    private void createMatch() {
        ServerMatch e = new ServerMatch(serv);
        e.start();
        int id = 0;
        while(checkId(id)) {
            id++;
        }
        e.id = id;
        matches.put(e.id, e);
    }
    
    private boolean checkId(int id) {
        if (matches.values().stream().anyMatch((cl) -> (cl.id == id))) {
            return true;
        }
        return false;
    }
    
    public static enum State {
        INGAME,LOBBY;
    }
}
