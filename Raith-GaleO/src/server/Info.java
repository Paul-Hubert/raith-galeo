package server;

import server.HubServer.State;
import lib.net.Server.ClientInfo;
import lib.phy.Vector;

public class Info implements java.io.Serializable {
    
    private final static Info f = new Info();
    
    public static Client newPlayerInfo() {
        return f.new Client();
    } public static Input newInput() {
        return f.new Input();
    } public static ShipInfo newShipInfo() {
        return f.new ShipInfo();
    }
    
    public class Input implements java.io.Serializable {
        public Vector AIM;
    }
    
    public class ShipInfo implements java.io.Serializable {
        public boolean UP = false,DOWN = false,LEFT = false,RIGHT = false,
                SHIFT = false,ENTER = false,SPACE = false,
                LCLICK = false,RCLICK = false;
        public Vector AIM = new Vector();
    }
    
    public class Client implements ClientInfo, java.io.Serializable {
        
        
        State state = State.LOBBY;
        int currentGame = -1;

        public void setInGame(int m) {
            currentGame = m;
            state = State.INGAME;
        }

        public void setToLobby() {
            currentGame = -1;
            state = State.LOBBY;
        }
    }
}
