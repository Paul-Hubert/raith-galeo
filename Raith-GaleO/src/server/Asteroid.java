/*
 * Copyright (C) 2015 moi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package server;

import lib.phy.Ball;
import lib.phy.Body;
import lib.phy.Vector;

/**
 *
 * @author moi
 */
public class Asteroid extends Ball {
    
    private int stage;
    
    public Asteroid(int stage, int r, Vector pp) {
        super(pp, r);
        this.stage = stage;
    }
    
    @Override
    public void collision(Vector dir, Body body) {
        
        if(body == null || !(body instanceof Particule)) return;
        
        stage--;
        kill();
        
        if(stage < 1) return;
        
        for(int i = 0; i < 4; i++) {
            double a = 2*Math.PI*Math.random();
            Vector d = new Vector((float) Math.cos(a), (float) Math.sin(a));
            Asteroid b = new Asteroid(stage, (int) (r/2), d.mult(r*1.5).add(this.p));
            
            b.m = stage*2;
            b.v = d.mult(100f).add(this.v);
            world.add(b);
        }
    }
    
}
