/*
 * Copyright (C) 2015 Popol99
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package server;

import lib.net.Packet;
import lib.net.Server;

/**
 *
 * @author Popol99
 */
public class ServerMatch extends Match {
    
    public transient int id = -1;
    public transient Server serv;
    
    public ServerMatch(Server serv) {
        super();
        this.serv = serv;
    }
    
    @Override
    public void start() {
        
        serv.setNetEvent("Input", (Packet p) -> {
            redirectPacket("keyPressed", p);
            Info.Input in = (Info.Input) p.obj;
            players.get(p.id).l.AIM = in.AIM;
        });
        serv.setNetEvent("keyPressed", (Packet p) -> {
            redirectPacket("keyPressed", p);
            this.keyPressed(p.id, (int) p.obj);
        });
        serv.setNetEvent("keyReleased", (Packet p) -> {
            redirectPacket("keyReleased", p);
            this.keyReleased(p.id, (int) p.obj);
        });
        serv.setNetEvent("mouseWheelMoved", (Packet p) -> {
            redirectPacket("mouseWheelMoved", p);
            this.mouseWheelMoved(p.id, (int) p.obj);
        });
        serv.setNetEvent("mousePressed", (Packet p) -> {
            redirectPacket("mousePressed", p);
            this.mousePressed(p.id, (int) p.obj);
        });
        serv.setNetEvent("mouseReleased", (Packet p) -> {
            redirectPacket("mouseReleased", p);
            this.mouseReleased(p.id, (int) p.obj);
        });
        
        super.start();
    }
    
    
    @Override
    public void sync() {
        sendToAll("Bodies", world.balls);
    }
    
    
    @Override
    public void addPlayer(int id) {
        sendToAll("newPlayer", id);
        super.addPlayer(id);
        sendTo(id, "Match", (Match) this);
    }
    
    @Override
    public void removePlayer(int id) {
        super.removePlayer(id);
        if(players.isEmpty()) {
            loopOn = false;
        }
        sendToAll("removePlayer", id);
    }
    
    public void sendToAll(String type, Object obj) {
        players.values().stream().forEach((s) -> {
            send(s.getID(),type,obj);
        });
    }
    
    public void sendTo(int id, String type, Object obj) {
        send(id,type,obj);
    }
    
    public void redirectPacket(String type, Packet p) {
        players.values().stream().forEach((s) -> {
            serv.con.get(s.getID()).send(p.id,p.type,p.obj);
        });
    }
    
    public final void send(int id, String type, Object obj) {
        serv.con.get(id).send(type, obj);
    }
}
