package server;

import java.awt.event.KeyEvent;
import java.util.HashMap;
import lib.net.Server;
import lib.phy.Ball;
import lib.phy.Body;
import lib.phy.Vector;
import lib.phy.Wall;
import lib.phy.World;

public class Match implements java.io.Serializable {
    
    public World world;
    
    public static final int PLAYER_LIMIT = 8;
    public boolean loopOn = true;
    
    private Vector spawn = new Vector(10,10);
    
    private Wall e;
    
    public HashMap<Integer,Ship> players = new HashMap<>();
    
    public Match() {
        world = new World();
        
        e = new Wall(world);
        world.walls.add(e);
        
        World.gravity = new Vector(0f,0f);
        world.SaveWorld("world.gw");
    }
    
    public Match(Match m) {
        world = m.world.World();
        loopOn = m.loopOn;
        spawn = m.spawn;
        e = m.e;
        players = m.players;
        
        world.walls.add(e);
        
        World.gravity = new Vector(0f,0f);
        world.SaveWorld("world.gw");
    }
    
    
    public void start() {
        
        world.start();
        
        Server.SThread("Match Thread",() -> {loop();});
    }
    
    public void loop() {
        long tim;
        
        while(loopOn) {
            tim = System.nanoTime();
            
            //sync();
            
            float accel = 400f;
            float friction = 0.3f;
            
            for(Ship s : players.values()) {
                
                if(s.l.SHIFT) {
                    accel *= 2;
                } if(s.l.UP) {
                    s.v.add(new Vector(0,-accel));
                } if(s.l.DOWN) {
                    s.v.add(new Vector(0,accel));
                } if(s.l.LEFT) {
                    s.v.add(new Vector(-accel,0));
                } if(s.l.RIGHT) {
                    s.v.add(new Vector(accel,0));
                } if(s.l.LCLICK) {
                    Vector dir = s.v.norm();
                    Particule b = new Particule(dir.mult(s.r+5).add(s.p), 3);
                    b.m = 50;
                    b.v = dir.mult(2000f).add(s.v);
                    world.balls.add(b);
                }
                s.v.sub(new Vector(s.v.x * friction, s.v.y  * friction));
                
                accel = 400f;
            }
            
            try {
                
                
                Thread.sleep(50 - (System.nanoTime() - tim) / 1000000000);
            } catch (InterruptedException ex) {
                System.err.print(ex);
            }
        }
    }
    
    public void sync() {}
    
    public void addPlayer(int id) {
        Ship s = new Ship(new Vector(),id);
        spawn(s);
        players.put(s.getID(), s);
        world.add(s);
    }
    
    public void removePlayer(int id) {
        world.balls.remove(players.get(id));
        players.remove(id);
    }
    
    public void keyPressed(int id, int key) {
        Ship s = players.get(id);
        if(key == KeyEvent.VK_SHIFT) {
            s.l.SHIFT = true;
        } if(key == KeyEvent.VK_Z) {
            s.l.UP = true;
        } if(key == KeyEvent.VK_S) {
            s.l.DOWN = true;
        } if(key == KeyEvent.VK_Q) {
            s.l.LEFT = true;
        } if(key == KeyEvent.VK_D) {
            s.l.RIGHT = true;
        } if(key == KeyEvent.VK_SPACE) {
            s.l.SPACE = true;
        }
        
        if(key == KeyEvent.VK_N) {
            e = new Wall(world);
            world.walls.add(e);
        } if(key == KeyEvent.VK_U) {
            s.setR(s.r+5);
        } if(key == KeyEvent.VK_I) {
            s.setR(s.r-5);
        } if(key == KeyEvent.VK_B) {
            world.addAction((World w) -> {w.add(new Ball(new Vector(s.p).add(s.l.AIM), (int) (10 + 10*Math.random())));});
        } if(key == KeyEvent.VK_V) {
            world.SaveWorld("world.gw");
        } if(key == KeyEvent.VK_SPACE) {
            float dir = s.v.angle();
            
            dir -= Math.PI/12;
            
            for(int i = 0; i < 5; i++) {
                Vector d = new Vector((float) Math.cos(dir), (float) Math.sin(dir));
                Particule b = new Particule(d.mult(s.r+5).add(s.p), 3);
                
                b.m = 50;
                b.v = d.mult(2000f).add(s.v);
                world.balls.add(b);
                
                dir += Math.PI/24;
            }
        } if(key == KeyEvent.VK_Y) {
            world.add(new Asteroid(4, 50, new Vector(s.p).add(s.l.AIM)));
        }
    }
    
    public void keyReleased(int id, int key) {
        Ship s = players.get(id);
        
        if(key == KeyEvent.VK_SHIFT) {
            s.l.SHIFT = false;
        } if(key == KeyEvent.VK_Z) {
            s.l.UP = false;
        } if(key == KeyEvent.VK_S) {
            s.l.DOWN = false;
        } if(key == KeyEvent.VK_Q) {
            s.l.LEFT = false;
        } if(key == KeyEvent.VK_D) {
            s.l.RIGHT = false;
        } if(key == KeyEvent.VK_SPACE) {
            s.l.SPACE = false;
        }
    }
    
    public void mouseWheelMoved(int id, int amount) {
        Ship s = players.get(id);
    }
    
    public void mousePressed(int id, int button) {
        Ship s = players.get(id);
        if(button == 1) {
            s.l.LCLICK = true;
        }
        if(button == 3) {
            s.l.RCLICK = true;
            e.addPoint((int) (s.p.x + s.l.AIM.x), (int) (s.p.y + s.l.AIM.y));
        }
    }
    
    public void mouseReleased(int id, int button) {
        Ship s = players.get(id);
        if(button == 1) {
            s.l.LCLICK = false;
        }
        if(button == 3) {
            s.l.RCLICK = false;
        }
    }
    
    public int getSize() {
        return players.size();
    }
    
    public void spawn(Body play) {
        Ship p = (Ship) play;
        p.setLocation(spawn);
    }
}
