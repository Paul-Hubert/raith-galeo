package server;

import lib.gui.Windu;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import lib.phy.Ball;
import lib.phy.Body;
import lib.phy.Vector;
import lib.phy.World;

public class Ship extends Ball {
    
    static final int PADNUM = 12;
    static final AlphaComposite AL1, AL0;
    static final double[] ANGS;
    static final Vector[] VECS;
    static final float PW,PH;
    
    static {
        PW = 15;
        PH = 5;
        AL1 = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1);
        AL0 = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0);
        ANGS = new double[PADNUM];
        VECS = new Vector[PADNUM];
        for(int i = 0; i<PADNUM; i++) {
            ANGS[i] = (Math.PI*2)*i/PADNUM;
            VECS[i] = new Vector((float) Math.cos(ANGS[i]), (float) Math.sin(ANGS[i]));
        }
    }
    
    float[] alphas = new float[PADNUM];
    private final int id;
    public Info.ShipInfo l = Info.newShipInfo();
    public double a;
    
    public Ship(Vector v, int ID) {
        super(v,40);
        this.id = ID;
        a = 0;
        for(int i = 0; i<PADNUM; i++) {
            alphas[i] = 0;
        }
    }
    
    public void setLocation(Vector v) {
        this.p = new Vector(v);
    }
    
    public int getID() {
        return id;
    }
    
    @Override
    public void draw(Graphics2D g, Windu win) {
        g.setColor(Color.BLACK);
        //int r2 = r / 2;
        //Vector v2 = World.toWinduVector(p, win);
        //g.fillOval((int) (v2.x - r2), (int) (v2.y - r2), r2 * 2, r2 * 2);
        Vector s = world.toWinduVector(p, win);
        
        update();
        
        g.rotate(a, (int) (s.x), (int) (s.y));
        
        g.drawLine((int) s.x-15,(int) s.y-15,(int) s.x+20,(int) s.y);
        g.drawLine((int) s.x-15,(int) s.y+15,(int) s.x+20,(int) s.y);
        g.drawLine((int) s.x-15,(int) s.y-15,(int) s.x-15,(int) s.y+15);
        
        g.rotate(-a, (int) (s.x), (int) (s.y));
        
        for(int i = 0; i<PADNUM; i++) {
            Vector f = new Vector().add(Ship.VECS[i]).mult(r - Ship.PH / 2).add(s);
            
            g.rotate(Ship.ANGS[i] + Math.PI / 2,f.x,f.y);
            if(alphas[i] == 0f) {
                g.setComposite(Ship.AL0);
            } else if(alphas[i] == 1f) {
                g.setComposite(Ship.AL1);
            } else {
                AlphaComposite alcom = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alphas[i]);
                g.setComposite(alcom);
            }
            g.setColor(Color.BLUE);
            
            g.fillRoundRect((int) (f.x - Ship.PW / 2), (int) (f.y - Ship.PH / 2), (int) Ship.PW, (int) Ship.PH, 5, 5);
            g.rotate(-(Ship.ANGS[i] + Math.PI / 2),f.x,f.y);
        }
        
        g.setComposite(Ship.AL1);
    }
    
    public void update() {
        a = Math.atan2(v.y,v.x);
    } 
    
    @Override
    public void collision(Vector p2, Body body) {
        for(int i = 0; i<PADNUM; i++) {
            Vector pp = Ship.VECS[i].mult(r - Ship.PH / 2);
            float d = Vector.dist2(pp, p2);
            if(d < (PW * PW * 4)) {
                alphas[i] = 1 - d / (PW * PW * 4) ;
            }
        }
    }
    
    @Override
    public void step(float tps) {
        for(int i = 0; i<PADNUM; i++) {
            alphas[i] -= 2 * alphas[i] * tps / 1000;
            if(alphas[i] < 0) {
                alphas[i] = 0;
            }
        }
    }
}
