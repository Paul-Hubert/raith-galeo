package pak;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Servlet extends HttpServlet {
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String lang = request.getParameter("lang");
        
        if(lang == null) {
            this.getServletContext().getRequestDispatcher("/index.html").forward(request, response);
        }
        
        lang = lang.toLowerCase();
        
        if(lang.contains("en")) {
            lang = "en";
        } else {
            lang = "fr";
        }
        
        request.setAttribute("lang", lang);
        
        this.getServletContext().getRequestDispatcher("/WEB-INF/form.jsp").forward(request, response);
    }
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String name = request.getParameter("name");
        String age = request.getParameter("age");
        String gender = request.getParameter("gender");
        String instru = request.getParameter("instru");
        String ress = request.getParameter("ress");
        
        TestJDBC test = new TestJDBC();
        
        String NAME = name.trim();
        int AGE = Integer.parseInt(age);
        boolean GENDER = gender.equals("guy");
        int INSTRU = Integer.parseInt(instru);
        String RESS = ress;
        try {
            test.addLine(NAME,AGE,GENDER,INSTRU,RESS);
        } catch (SQLException ex) {
            response.setContentType("text/html");
            response.setCharacterEncoding("UTF-8");
            PrintWriter out = response.getWriter();
            out.println(ex);
        }
    }
}
