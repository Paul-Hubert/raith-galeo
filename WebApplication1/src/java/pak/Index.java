package pak;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Index extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String lang = request.getParameter("lang");
        
        if(lang == null) {
            this.getServletContext().getRequestDispatcher("/index.html").forward(request, response);
            return;
        }
        
        lang = lang.toLowerCase();
        
        if(lang.contains("en")) {
            lang = "en";
        } else {
            lang = "fr";
        }
        
        request.setAttribute("lang", lang);
        
        this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
    }
}
