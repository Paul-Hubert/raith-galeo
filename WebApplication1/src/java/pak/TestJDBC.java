
package pak;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestJDBC {
    String url = "jdbc:derby://localhost:1527/contact";
    String utilisateur = "APP";
    String motDePasse = "APP";
    Connection connexion = null;
    
    //CREATE TABLE ENTRY (id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "NAME" VARCHAR(30) NOT NULL, AGE SMALLINT NOT NULL, Gender BOOLEAN NOT NULL, Instrument SMALLINT NOT NULL, Data VARCHAR(100) NOT NULL, CONSTRAINT primary_key PRIMARY KEY (id));
    //export DERBY_INSTALL=~/glassfish4/javadb/
    //export CLASSPATH=$DERBY_INSTALL/lib/derby.jar:$DERBY_INSTALL/lib/derbytools.jar:
    //java org.apache.derby.tools.ij
    //glassfish4/bin/asadmin *----*
    //vps217991.ovh.net
    //Ao11zbLK
    
    public TestJDBC() {
        try {
            Class.forName( "org.apache.derby.jdbc.EmbeddedDriver" );
        } catch ( ClassNotFoundException e ) {
            System.err.print(e);
        }
    }
    
    
    public void addLine(String name, int age, boolean gender, int instru, String ress) throws SQLException {
        Statement statement = null;
        try {
            connexion = DriverManager.getConnection(url, utilisateur, motDePasse);
            statement = connexion.createStatement();
            statement.executeUpdate("INSERT INTO ENTRY (Name, Age, Gender, Instrument, Data) VALUES ('" + name + "'," + age + ", " + gender + ", " + instru + ", '" + ress + "')");
        } catch ( SQLException e ) {
            throw e;
        } finally {
            if ( statement != null ) {
                try {
                    statement.close();
                } catch ( SQLException ignore ) {}
            }
            if ( connexion != null )
                try {
                    connexion.close();
                } catch ( SQLException ignore ) {}
        }
    }
    
    public void removeLine(int id) throws SQLException {
        Statement statement = null;
        try {
            connexion = DriverManager.getConnection(url, utilisateur, motDePasse);
            statement = connexion.createStatement();
            statement.executeUpdate("DELETE FROM Entry WHERE ID="+ id);
        } catch (SQLException e) {
            throw e;
        } finally {
            if ( statement != null ) {
                try {
                    statement.close();
                } catch ( SQLException ignore ) {}
            }
            if ( connexion != null )
                try {
                    connexion.close();
                } catch ( SQLException ignore ) {}
        }
    }

    public List<Entry> getAllResults() throws SQLException {
        Statement statement = null;
        ResultSet resultat = null;
        List<Entry> list = new ArrayList<>();
        try {
            connexion = DriverManager.getConnection(url, utilisateur, motDePasse);
            statement = connexion.createStatement();
            /* Exécution d'une requête de lecture */
            resultat = statement.executeQuery( "SELECT * FROM ENTRY" );
            
            /* Récupération des données du résultat de la requête de lecture */
            while (resultat.next()) {
                Entry e = Entry.getEntry(resultat);
                if(e != null) list.add(e);
            }
        } catch ( SQLException e ) {
            throw(e);
        } finally {
            if ( resultat != null ) {
                try {
                    resultat.close();
                } catch ( SQLException ignore ) {
                }
            }
            if ( statement != null ) {
                try {
                    statement.close();
                } catch ( SQLException ignore ) {}
            }
            if ( connexion != null )
                try {
                    connexion.close();
                } catch ( SQLException ignore ) {}
        }
        
        return list;
    }
    
    public void clean() throws SQLException {
        
        
        List<Entry> list = getAllResults();
        
        Statement statement = null;
        try {
            connexion = DriverManager.getConnection(url, utilisateur, motDePasse);
            statement = connexion.createStatement();
            
            int[] moyennes = new int[list.size()];
            
            System.out.println("Cleaning data");
            
            for(Entry e : list) {
                moyennes[list.indexOf(e)] = e.getMoy();
                System.out.println("----" + e.getId());
                double median, mean, MAD;
                
                int[] array = e.getData();
                Arrays.sort(array);
                
                if(array.length % 2 == 1) {
                    median = (array[array.length/2-1] + array[array.length/2+1]) / 2;
                } else {
                    median = array[array.length/2];
                }
                
                
                mean = 0;
                for(int n : array) {
                    mean+=n;
                }
                mean/=array.length;
                
                
                MAD = 0;
                for(int n : array) {
                    MAD+=Math.abs(n - mean);
                }
                MAD/=array.length;
                
                System.out.println(median);
                System.out.println(mean);
                System.out.println(MAD);
                
                for(int n : array) {
                    double Z = Math.abs(0.6745d * (n - median) / MAD);
                    if(Z>3.5) {
                        System.out.println("---Outlier--- id=" + e.getId() + " value=" + n);
                        String newdata = "";
                        
                        for(int i = 0; i<array.length; i++) {
                            int a = array[i];
                            if(a != n) {
                                if(i > 0) newdata+=",";
                                newdata+=a;
                            }
                        }
                        
                        System.out.println(newdata);
                        
                        statement.executeUpdate("UPDATE Entry SET DATA='" + newdata + "' WHERE ID="+ e.getId());
                    } else {
                        System.out.println(n + " : " + Z);
                    }
                }
            }
            
            System.out.println("Cleaning lines");
            
            double median, mean, MAD;
            
            Arrays.sort(moyennes);

            if(moyennes.length % 2 == 1) {
                median = (moyennes[moyennes.length/2-1] + moyennes[moyennes.length/2+1]) / 2;
            } else {
                median = moyennes[moyennes.length/2];
            }


            mean = 0;
            for(int n : moyennes) {
                mean+=n;
            }
            mean/=moyennes.length;


            MAD = 0;
            for(int n : moyennes) {
                MAD+=Math.abs(n - mean);
            }
            MAD/=moyennes.length;

            System.out.println(median);
            System.out.println(mean);
            System.out.println(MAD);

            for(int i = 0; i<moyennes.length; i++) {
                int n = moyennes[i];
                double Z = Math.abs(0.6745d * (n - median) / MAD);
                if(Z>3.5) {
                    System.out.println("---Outlier--- id=" + list.get(i).getId() + " value=" + n + " Z=" + Z);
                    
                    statement.executeUpdate("DELETE FROM Entry WHERE ID="+ list.get(i).getId());
                } else {
                    System.out.println(n + " : " + Z);
                }
            }
            
            
            //statement.executeUpdate("UPDATE Entry SET DATA='" + newdata + "' WHERE ID="+ id);
            
        } catch (SQLException e) {
            throw e;
        } finally {
            if ( statement != null )
                try {
                    statement.close();
                } catch ( SQLException ignore ) {}
            if ( connexion != null )
                try {
                    connexion.close();
                } catch ( SQLException ignore ) {}
        }
    }
}
