package pak;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Entry {
    private int id;
    private String name;
    private int age;
    private boolean gender;
    private int instru;
    private int[] data;
    private int moy;
    private int rmoy;
    private int amoy;
    private int et;
    
    public Entry() {}
    
    public static Entry getEntry(ResultSet set) {
        Entry e = new Entry();
        
        try {
            e.id = set.getInt("id");
            e.name = set.getString("name");
            e.age = set.getInt("age");
            e.gender = set.getBoolean("gender");
            e.instru = set.getInt("instrument");
            String[] strs = set.getString("data").split(",");
            
            e.data = new int[strs.length];
            
            for(int i = 0;i<e.data.length;i++) {
                e.data[i] = Integer.parseInt(strs[i]);
            }
            
            int m = 0;
            for(int i = 0;i<e.data.length;i++) {
                int c = e.data[i];
                m+=c;
            }
            m = m/e.data.length;
            e.moy = Math.round(m);
            e.rmoy = e.moy + 1000;
            e.amoy = Math.abs(e.moy);
            double ecty = 0d;
            for(int i = 0;i<e.data.length;i++) {
                int c = m-e.data[i];
                ecty+=c*c;
            }
            ecty /= e.data.length;
            ecty = Math.sqrt(ecty);
            e.et = (int) Math.round(ecty);
            
        } catch (SQLException | NumberFormatException ex) {
            return null;
        }
        return e;
    }
    
    
    
    @Override
    public String toString() {return id + name + age + gender;}
    public int getId() {return id;}
    public void setId(int id) {this.id = id;}
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    public int getAge() {return age;}
    public void setAge(int age) {this.age = age;}
    public boolean isGender() {return gender;}
    public void setGender(boolean gender) {this.gender = gender;}
    public int getInstru() {return instru;}
    public void setInstru(int instru) {this.instru = instru;}
    public int[] getData() {return data;}
    public void setData(int[] data) {this.data = data;}
    public int getMoy() {return moy;}
    public void setMoy(int moy) {this.moy = moy;}
    public int getRmoy() {return rmoy;}
    public void setRmoy(int rmoy) {this.rmoy = rmoy;}
    public int getAmoy() {return amoy;}
    public void setAmoy(int amoy) {this.amoy = amoy;}
    public int getEt() {return et;}
    public void setEt(int et) {this.et = et;}
}
