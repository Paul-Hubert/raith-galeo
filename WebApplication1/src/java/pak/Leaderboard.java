package pak;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Leaderboard extends HttpServlet {
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        TestJDBC test = new TestJDBC();
        
        List<Entry> list;
        
        try {
            list = test.getAllResults();
        } catch (SQLException ex) {
            response.setContentType("text/html");
            response.setCharacterEncoding("UTF-8");
            PrintWriter out = response.getWriter();
            out.println(ex);
            return;
        }
        
        List<Entry> temp = new ArrayList<>();
        
        Entry c;
        
        int l = list.size();
        
        for(int i = 0; i<l; i++) {
            int highest = 0;
            
            for(int j = 0;j<list.size();j++) {
                if( list.get(j).getAmoy() + list.get(j).getEt() < list.get(highest).getAmoy() + list.get(highest).getEt() ) {
                    highest = j;
                }
            }
            
            c = list.get(highest);
            c.setId(i+1);
            temp.add(c);
            list.remove(c);
        }
        
        request.setAttribute( "entries", temp);
        
        String lang = request.getParameter("lang");
        
        if(lang == null) {
            lang = "fr";
        }
        
        lang = lang.toLowerCase();
        
        if(lang.contains("en")) {
            lang = "en";
        } else {
            lang = "fr";
        }
        
        request.setAttribute("lang", lang);
        
        this.getServletContext().getRequestDispatcher("/WEB-INF/leaderboard.jsp").forward(request, response);
    }
}
