package pak;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Admin extends HttpServlet {
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        if(request.getParameter("pw") == null || !request.getParameter("pw").equals("train")) {
            response.setContentType("text/html");
            response.setCharacterEncoding("UTF-8");
            PrintWriter out = response.getWriter();
            out.println("Mot de passe requis");
            return;
        }
        
        TestJDBC test = new TestJDBC();
        
        List<Entry> list;
        
        try {
            list = test.getAllResults();
        } catch (SQLException ex) {
            response.setContentType("text/html");
            response.setCharacterEncoding( "UTF-8" );
            PrintWriter out = response.getWriter();
            out.println(ex);
            return;
        }
        
        
        request.setAttribute( "entries", list);
        this.getServletContext().getRequestDispatcher("/WEB-INF/test.jsp").forward(request, response);
    }
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        System.out.println("post");
        
        if(request.getParameter("pw") == null || !request.getParameter("pw").equals("train")) {
            response.setContentType("text/html");
            response.setCharacterEncoding("UTF-8");
            PrintWriter out = response.getWriter();
            out.println("Mot de passe requis");
            return;
        }
        
        if(request.getParameter("action").equals("del")) {
            
            
            int id = Integer.parseInt(request.getParameter("id"));
            
            TestJDBC test = new TestJDBC();
            
            try {
                test.removeLine(id);
            } catch (SQLException ex) {
                response.setCharacterEncoding( "UTF-8" );
                PrintWriter out = response.getWriter();
                out.println(ex);
            }
            
            List<Entry> list;
            
            try {
                list = test.getAllResults();
            } catch (SQLException ex) {
                response.setContentType("text/html");
                response.setCharacterEncoding( "UTF-8" );
                PrintWriter out = response.getWriter();
                out.println(ex);
                return;
            }
            
            request.setAttribute( "entries", list);
            this.getServletContext().getRequestDispatcher("/WEB-INF/test.jsp").forward(request, response);
            
        } else if(request.getParameter("action").equals("clean")) {
            TestJDBC test = new TestJDBC();
            
            try {
                test.clean();
            } catch (SQLException ex) {
                response.setCharacterEncoding( "UTF-8" );
                PrintWriter out = response.getWriter();
                out.println(ex);
            }
            
            List<Entry> list;
            
            try {
                list = test.getAllResults();
            } catch (SQLException ex) {
                response.setContentType("text/html");
                response.setCharacterEncoding( "UTF-8" );
                PrintWriter out = response.getWriter();
                out.println(ex);
                return;
            }
            
            request.setAttribute( "entries", list);
            this.getServletContext().getRequestDispatcher("/WEB-INF/test.jsp").forward(request, response);
        }
    }
}
