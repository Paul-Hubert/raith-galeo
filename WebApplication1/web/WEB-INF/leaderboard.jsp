<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport"/>
        <meta lang="${lang}"/>
        <title>Mesure de la Seconde</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <style>
            body {
                background: url('${pageContext.request.contextPath}/img/debut_light.png');
                background-repeat:repeat;
                font-size: 18px;
            }
        </style>
    </head>
    
    <body>
        <div class='container text-center'>
        <legend><h1><b>${lang=='en'? 'Second Timing':'Mesure de la seconde'}</b></h1></legend>
        <table class="table table-striped table-bordered table-hover">
            
            <tr>
                <th>#</th>
                <th>${lang=='en'? 'Name':'Nom'}</th>
                <th>${lang=='en'? 'Average (in ms)':'Moyenne (en ms)'}</th>
                <th>${lang=='en'? 'Consistency (lower is better)':'Régularité (moins > plus)'}</th>
            </tr>
            
            <c:forEach var="entry" items="${entries}">
                <tr>
                    <td><c:out value="${entry.id}"/></td>
                    <td><c:out value="${entry.name}"/></td>
                    <td><c:out value="${entry.rmoy}"/></td>
                    <td><c:out value="${entry.et}"/></td>
                </tr>
            </c:forEach>
        </table>
        </div>
    </body>
</html>
