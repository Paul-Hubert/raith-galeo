<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport"/>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <title>Admin</title>
    </head>
    <body>
        <div class='container'>
        <legend><h1>Admin SQL</h1><button onclick="clean();" type="button" class="btn btn-default btn-xs">Clean (do not push)</button></legend>
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th>ID</th>
                <th>Nom</th>
                <th>Âge</th>
                <th>Sexe</th>
                <th>Inst</th>
                <th>Moy</th>
                <th>RMoy</th>
                <th>AMoy</th>
                <th>ET</th>
                <th>Del</th>
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5</th>
                <th>6</th>
                <th>7</th>
                <th>8</th>
                <th>9</th>
                <th>10</th>
            </tr>
            <c:forEach var="entry" items="${entries}">
                <tr>
                    <td><c:out value="${entry.id}"/></td>
                    <td><c:out value="${entry.name}"/></td>
                    <td><c:out value="${entry.age}"/></td>
                    <td><c:out value="${entry.gender}"/></td>
                    <td><c:out value="${entry.instru}"/></td>
                    <td><c:out value="${entry.moy}"/></td>
                    <td><c:out value="${entry.rmoy}"/></td>
                    <td><c:out value="${entry.amoy}"/></td>
                    <td><c:out value="${entry.et}"/></td>
                    <td><button onclick="del(${entry.id});" type="button" class="btn btn-default btn-xs glyphicon glyphicon-remove-sign"></button></td>
                    <c:forEach var="data" items="${entry.data}">
                        <td><c:out value="${data}"/></td>
                    </c:forEach>
                </tr>
                
            </c:forEach>
            <script>
                function del(id) {
                    var xhr = new XMLHttpRequest();
                    xhr.onload = function() {receive(xhr);};
                    xhr.open('POST', '/admin');
                    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xhr.send('pw=train&action=del&id='+id);
                }
                
                function clean() {
                    var xhr = new XMLHttpRequest();
                    xhr.onload = function() {receive(xhr);};
                    xhr.open('POST', '/admin');
                    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xhr.send('pw=train&action=clean');
                }
                
                function receive(xhr) {
                    var xhr2 = new XMLHttpRequest();
                    xhr2.open('GET', '/admin?pw=train',false);
                    xhr2.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xhr2.send();
                    document.open();
                    document.write(xhr2.responseText);
                    document.close();
                }
            </script>
        </table>
        </div>
    </body>
</html>
