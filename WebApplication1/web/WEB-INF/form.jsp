<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport"/>
        <meta lang="${lang}"/>
        <title>Mesure de la Seconde</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <style>
            body {
                background: url('${pageContext.request.contextPath}/img/debut_light.png');
                background-repeat:repeat;
                font-size: 18px;
            }
            .text-center {
                margin: auto;
                width: 45%;
            }
            .btn-block {
                margin: auto;
                width: 50%;
            }
        </style>
    </head>
    
    <body>
        <div id="postError"></div>
        <div id="formdiv" class="container text-center" >
            <legend><h1><b>${lang=='en'? 'Second Timing':'Mesure de la seconde'}</b></h1></legend>
            <p><h3>${lang=='en'? 'Please answer a few questions':'Veuillez repondre à quelques questions'}</h3></p>
            <form id="formbro" class='form form-inline'>
                <div id="nameDiv" class="">
                    <span class="help-block" id="nameError">${lang=='en'? 'What is your name ?':'Comment vous appelez-vous ?'}</span>
                    <input type="text" class="form-control" id="name" aria-describedby="nameError" placeholder="${lang=='en'? 'Name':'Nom'}"/>
                </div>
                
                <div id="ageDiv" class="">
                    <span class="help-block" id="ageError">${lang=='en'? 'How old are you ?':'Quel âge avez-vous ?'}</span>
                    <input type="number" class='form-control' id="age" aria-describedby="ageError" placeholder="${lang=='en'? 'Age':'Âge'}"/>
                </div>
                
                <div id="genderDiv" class=""><span class="help-block" id="genderError">${lang=='en'? 'Are you a Man or a Woman ?':'Êtes vous une femme ou un homme ?'}</span></div>
                
                <label class="radio-inline" for="guy">${lang=='en'? 'Man':'Homme'}</label>
                <input type="radio" id="guy" name="gender" value="guy" aria-describedby="genderError"/>
                
                <label class="radio-inline" for="girl">${lang=='en'? 'Woman':'Femme'}</label>
                <input type="radio" id="girl" name="gender" value="girl" aria-describedby="genderError"/>
                
                
                <div id="instruDiv" class=""><span class="help-block" id="instruError">${lang=='en'? 'Have you been playing music these days ?':'Jouez-vous de la musique ces temps-ci ?'}</span></div>
                
                <label class="radio-inline" for="instru0">${lang=='en'? 'No, never':'Non, jamais'}</label>
                <input type="radio" id="instru0" name="instru" value="guy" aria-describedby="instruError"/>
                
                <label class="radio-inline" for="instru1">${lang=='en'? 'Not recently':'Pas en ce moment'}</label>
                <input type="radio" id="instru1" name="instru" value="girl" aria-describedby="instruError"/>
                
                <label class="radio-inline" for="instru2">${lang=='en'? 'Yes':'Oui'}</label>
                <input type="radio" id="instru2" name="instru" value="girl" aria-describedby="instruError"/>
                <br/><br/>
                
                <button onclick="processForm();" type="button" class="btn btn-primary btn-lg btn-block">${lang=='en'? 'Continue':'Continuer'}&nbsp;<span class="glyphicon glyphicon-chevron-right"></span></button>
                <!--<a href='/score?lang=${lang}' type="button" class="btn btn-default btn-lg">${lang=='en'? 'See the scoreboard':'Voir les scores'}<span class="glyphicon glyphicon-chevron-right"></span></a>-->
            </form>
        </div>
        
        <script>
            function processForm() {
                var error = false;
                
                if (document.getElementById("name").value === null || document.getElementById("name").value === "") {
                    document.getElementById("nameDiv").getAttributeNode("class").value = "has-error";
                    error = true;
                } else {
                    document.getElementById("nameDiv").getAttributeNode("class").value = "";
                }
                if(document.getElementById("age").value === null || document.getElementById("age").value === "" || document.getElementById("age").value > 150) {
                    document.getElementById("ageDiv").getAttributeNode("class").value = "has-error";
                    error = true;
                } else {
                    document.getElementById("ageDiv").getAttributeNode("class").value = "";
                }
                
                var radios = document.getElementsByName('gender');
                var checked = false;
                for (var i = 0, length = radios.length; i < length; i++) {
                    if (radios[i].checked) {
                        document.getElementById("genderDiv").getAttributeNode("class").value = "";
                        checked = true;
                        break;
                    }
                }
                if(!checked) {
                    document.getElementById("genderDiv").getAttributeNode("class").value = "has-error";
                    error = true;
                }
                
                radios = document.getElementsByName('instru');
                checked = false;
                for (var i = 0, length = radios.length; i < length; i++) {
                    if (radios[i].checked) {
                        document.getElementById("instruDiv").getAttributeNode("class").value = "";
                        checked = true;
                        break;
                    }
                }
                if(!checked) {
                    document.getElementById("instruDiv").getAttributeNode("class").value = "has-error";
                    error = true;
                }
                
                if(!error) {
                    document.getElementById("formdiv").setAttribute("hidden", "hidden");
                    document.getElementById("rept").removeAttribute("hidden");
                }
            }
        </script>
        
        <div id="rept" class="container text-center" hidden>
            <legend><h1><b>${lang=='en'? 'Second Timing':'Mesure de la seconde'}</b></h1></legend>
            
            <p name="fr" ${lang=='en'? 'hidden':''}>
                Pour effectuer le test, cliquez sur le bouton dix fois à intervalles régulières. 
                Le but est de se rapprocher d'une intervalle de une seconde sans tricher... 
                Une fois terminé, cliquez sur envoyer. Si vous n'aviez pas compris et souhaitez réessayer, cliquez sur le petit bouton.
            </p>
            <p name="en" ${lang=='en'? '':'hidden'}>
                To complete the test, click the button ten times at a regular pace. 
                The goal is to keep a one second interval between clicks without cheating... 
                After that, click on send. If you didn't understand and wish to try again, click on the small button.
            </p>
            
            <div class="row">
                <button id="input" onclick="input()" type="button" class="btn btn-primary btn-lg btn-block">${lang=='en'? 'Click here':'Cliquez ici'}</button>
                <br>
                <button onclick="restart()" type="button" class="glyphicon glyphicon-repeat btn btn-default btn-xs"></button>&nbsp;<button id="next" onclick="next()" type="button" class="btn btn-default btn-lg" disabled>${lang=='en'? 'Send':'Envoyer'}&nbsp;<span class="glyphicon glyphicon-ok"></span></button>
            </div><br>
            
            ${lang=='en'? 'Progression':'Progression'} : &nbsp;<span id="num">0/10</span>
            <div id="res" hidden>
                ${lang=='en'? 'Average (in ms)':'Moyenne (en ms)'} : <span id="moy">0</span><br/>
                ${lang=='en'? 'Consistency (lower is better)':'Régularité (Moins > Plus)'} : <span id="ET">0</span>
            </div>
        <script>
            var tim = 0;
            var clicked = false;
            
            var moy = 0;
            var ET = 0;
            var testNum = 0;
            
            var ress = [];
            
            function input() {
                //second click - timer stop
                var date = Date.now();
                var res = date - tim;
                tim = date;
                if(testNum > 0) {
                    ress[testNum-1] = res-1000;
                    
                }
                testNum++;
                if(testNum >= 10) {
                    var m = 0;
                    for(var i = 0;i<ress.length;i++) {
                        m += ress[i];
                    }
                    m = m/ress.length;
                    moy = Math.round(m);
                    
                    var c = 0;
                    for(var i = 0; i<ress.length; i++) {
                        var t = moy-ress[i];
                        c+=t*t;
                    }
                    c/=ress.length;
                    ET = Math.round(Math.sqrt(c));
                    
                    document.getElementById("moy").innerHTML = moy+1000;
                    document.getElementById("ET").innerHTML = ET;
                    document.getElementById("res").removeAttribute("hidden");
                    document.getElementById("next").removeAttribute("disabled");
                    document.getElementById("input").setAttribute("disabled", "disabled");
                }
                document.getElementById("num").innerHTML = testNum + "/10";
            }
            
            function next() {
                if(testNum >= 10 && !clicked) {
                    clicked = true;
                    send();
                }
            }
            
            function restart() {
                tim = 0;
                clicked = false;
                moy = 0;
                ET = 0;
                testNum = 0;
                ress = [];
                
                document.getElementById("num").innerHTML = testNum + "/10";
                document.getElementById("moy").innerHTML = moy+1000;
                document.getElementById("ET").innerHTML = ET;
                document.getElementById("res").setAttribute("hidden", "hidden");
                document.getElementById("next").setAttribute("disabled", "disabled");
                document.getElementById("input").removeAttribute("disabled");
            }
            
            function send() {
                var xhr = new XMLHttpRequest();
                xhr.onload = function() {receive(xhr);};
                xhr.open('POST', '/form');
                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                
                var name = document.getElementById("name").value;
                var age = document.getElementById("age").value;
                var gender;
                if(document.getElementById("guy").checked) {
                    gender = "guy";
                } else {
                    gender = "girl";
                }
                var instru;
                if(document.getElementById("instru0").checked) {
                    instru = "0";
                } else if(document.getElementById("instru1").checked) {
                    instru = "1";
                } else {
                    instru = "2";
                }
                
                name = encodeURIComponent(name);
                age = encodeURIComponent(age);
                gender = encodeURIComponent(gender);
                instru = encodeURIComponent(instru);
                ress = encodeURIComponent(ress);
                
                xhr.send('name='+name+'&age='+age+'&gender='+gender+'&instru='+instru+'&ress='+ress);
            }
            
            function receive(xhr) {
                window.location = "/score?lang=${lang}";
            }
        </script>
        </div>
    </body>
</html>
