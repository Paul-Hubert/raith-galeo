<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<!--
Copyright (C) 2015 moi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<html>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport"/>
        <meta lang="${lang}"/>
        <title>Mesure de la Seconde</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <style>
            body {
                background: url('${pageContext.request.contextPath}/img/debut_light.png');
                background-repeat:repeat;
                font-size: 18px;
            }
            .text-center {
                margin: auto;
                width: 45%;
            }
            .btn-block {
                margin: auto;
                width: 50%;
            }
        </style>
    </head>
    
    <body>
        <div class="container text-center">
            <legend><h1><b>${lang=='en'? 'Second Timing':'Mesure de la seconde'}</b></h1></legend>
            <p name="fr" ${lang=='en'? 'hidden':''}>
                On est une équipe de 4 premières S travaillant pour notre <a href='http://eduscol.education.fr/cid47789/themes-nationaux.html'>tpe</a>
                sur la capacité du cerveau à se rappeler et ressortir un lapse de temps, dans ce cas, une seconde. <br/><br/>
                Le but de ce site est de récupérer autant de données que possible sur les gens et leur concept de la seconde ; 
                et pour cela, nous avons besoin de <b>vous ! </b>
            </p>
            <p name="en" ${lang=='en'? '':'hidden'}>
                We're a team of 4 french 11th graders (16/17 years old) working on a school project (<a href='http://eduscol.education.fr/cid47789/themes-nationaux.html'>tpe</a>)
                on the brain's high level capacity to remember and retranslate a time lapse, in this case, a second. <br/><br/>
                The aim of this website is to collect as much data possible on people and their concept of a second ; 
                and for this we need <b>you ! </b>
            </p>
            
            <a href='/form?lang=${lang}' type="button" class="btn btn-primary btn-lg btn-block">${lang=='en'? 'Continue':'Continuer'}&nbsp;<span class="glyphicon glyphicon-chevron-right"></span></a>
            
            <br/>
            <div name="fr" ${lang=='en'? 'hidden':''}>
                Developpé par Paul Boursin, avec l'aide de Geoffrey Lord, Paul Blalack, et Liam Imadache.
            </div>
            <div name="en" ${lang=='en'? '':'hidden'}>
                Developed by Paul Boursin, with the help of Geoffrey Lord, Paul Blalack, and Liam Imadache.
            </div>
            <a href='/index?lang=${lang=='en'? 'fr':'en'}' type="button" class="btn btn-default btn-xs">${lang=='en'? 'Français':'English'}<span class="glyphicon glyphicon-chevron-right"></span></a>
            
        </div>
    </body>
</html>
