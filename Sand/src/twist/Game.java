package twist;

import phy.Box;
import phy.World;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import phy.Particule;
import phy.Vector;

public class Game extends Windu {
    
    public static void main(String[] args) {
        sp = new Game("Twist",true);
        try {
            sp.loop();
        } catch (InterruptedException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    static Game sp;
    public World world;
    Random ran = new Random();
    boolean loopOn = true;
    boolean UP,DOWN,LEFT,RIGHT,LCLICK,RCLICK,SPACE;
    
    public Game(String str, boolean full) {
        super(str, full);
        world = new World(new Box(-500,-300,1000,600));
        
        //world.terrain.add(new Particule(new Vector(0f,0f),40));
        //world.terrain.add(new Particule(new Vector(0f,200f),50));
        
//        SaveMap("world.wld");
//        LoadMap("world.wld");
        
        open();
    }
    
    
    private void loop() throws InterruptedException {
        new Thread(() -> {input();}).start();
        long tim = System.nanoTime();
        
        while(loopOn) {
            float tps = System.nanoTime() - tim;
            tim = System.nanoTime();
            if(!SPACE) world.work(tps / 1000000);
            if(tps == 0) Thread.sleep(1);
        }
        SaveMap("world.wld");
    }
    
    
    
    private void input() {
        while(loopOn) {
            if(LCLICK) {
                Particule particule;
                if(RIGHT) {
                    particule = new Particule(screenToWorld(mouse()), 10, 40);
                    LCLICK = false;
                } else if(UP) {
                    particule = new Particule(screenToWorld(mouse()), 50, 4000);
                    LCLICK = false;
                } else if(LEFT) {
                    int r = (int) (5+ran.nextFloat()*5);
                    particule = new Particule(screenToWorld(mouse()), r, r);
                    LCLICK = false;
                } else {
                    int r = (int) (10+ran.nextFloat()*5);
                    particule = new Particule(screenToWorld(mouse()), r, r);
                    LCLICK = false;
                }
                world.terrain.add(particule);
            }
            
            if(RCLICK) {
                world.grab(screenToWorld(mouse()));
                RCLICK = false;
            }
            
            if(DOWN) {
//                System.out.println("Total Energy : " + world.getTotalEnergy());
                World.gravity = !World.gravity;
                DOWN = false;
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
    
    
    
    @Override
    public void Draw() {
        
        
        synchronized(world.terrain) {
            world.draw(this);
        }
    }
    
    
    
    
    
    public Vector screenToWorld(Vector p) {
        p.x += cx - size.width / 2;
        p.y += cy - size.height / 2;
        return p;
    }
    
    public Vector worldToScreen(Vector p) {
        p.x -= cx - size.width / 2;
        p.y -= cy - size.height / 2;
        return p;
    }
    
    private boolean LoadMap(String mapLocation) {
        File worldInfo = new File(mapLocation);
        FileInputStream file;
        ObjectInputStream obj = null;
        try {
            file = new FileInputStream(worldInfo);
            obj = new ObjectInputStream(file);
            world = (World) obj.readObject();
        } catch (IOException ex) {
            System.out.println("Can't find " + mapLocation);
            System.out.println(worldInfo.exists());
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (ClassNotFoundException ex) {
            System.out.println("Class World not found");
            return false;
        }
        return true;
    }
    
    private void SaveMap(String mapLocation) {
        System.out.println(world.terrain.size() + " particules");
        File worldInfo = new File(mapLocation);
        if(worldInfo.exists()) {
            worldInfo.delete();
        }
        
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = null;
        FileOutputStream file = null;
        
        try {
            file = new FileOutputStream(worldInfo);
            out = new ObjectOutputStream(bos);
            out.writeObject(world);
            System.out.println(bos.toByteArray().length);
            bos.writeTo(file);

        } catch (IOException ex) {
            System.out.println("Something happened--serialize()");
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                
            } catch (IOException ex) {
                System.out.println("Something happened--serialize()");
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                bos.close();
            } catch (IOException ex) {
                System.out.println("Something happened--serialize()");
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if (file != null) {
                try {
                    file.close();
                } catch (IOException ex) {
                    System.out.println("Something happened--serialize()");
                    Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
