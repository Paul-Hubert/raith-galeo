package twist;

import phy.Box;
import phy.Vector;
import java.awt.Dimension;
import java.awt.MouseInfo;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.BufferUtils;
import org.lwjgl.Sys;
import static org.lwjgl.glfw.Callbacks.errorCallbackPrint;
import org.lwjgl.glfw.GLFW;
import static org.lwjgl.glfw.GLFW.*;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import static twist.Game.sp;

public class Windu implements Runnable {
    
    Thread draw;
    boolean drawOnWindow;
    
    public Dimension size;
    float cx, cy;
    // We need to strongly reference callback instances.
    private int ballProgram, lightProgram, vao1,vao2,vbo1,vbo2,uboid;
    
    // The window handle
    public long window;
    private GLFWScrollCallback scb;
    private GLFWMouseButtonCallback mcb;
    private GLFWKeyCallback kcb;
    private GLFWErrorCallback ecb;
    private GLFWWindowSizeCallback wcb;
    
    public Windu(String str, boolean full) {
        cx = 0;
        cy = 0;
        draw = new Thread(this);
    }
    
    public void init() {
        System.out.println("Hello LWJGL " + Sys.getVersion() + "!");
        // Setup an error callback. The default implementation
        // will print the error message in System.err.
        glfwSetErrorCallback(ecb = errorCallbackPrint(System.err));
        
        // Initialize GLFW. Most GLFW functions will not work before doing this.
        if ( glfwInit() != GL11.GL_TRUE ) throw new IllegalStateException("Unable to initialize GLFW");
        
        ByteBuffer vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        
        GLFW.glfwDefaultWindowHints();
        GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GL_FALSE);
        GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, GL_TRUE);
        
        // Create the window
        window = glfwCreateWindow(GLFWvidmode.width(vidmode), GLFWvidmode.height(vidmode), "My Title", glfwGetPrimaryMonitor(), 0);
        // Setup a key callback. It will be called every time a key is pressed, repeated or released.
        
        size = new Dimension(GLFWvidmode.width(vidmode), GLFWvidmode.height(vidmode));
        System.out.println(size);
        
        // Make the OpenGL context current
        glfwMakeContextCurrent(window);
        // Enable v-sync
        glfwSwapInterval(1);
        
        GL.createCapabilities();
        
        System.out.println("OpenGL version: " + GL11.glGetString(GL11.GL_VERSION));
        System.out.println(GL.getCapabilities().GL_EXT_framebuffer_object);
        prepareShaders();
        
        glfwSetKeyCallback(window, kcb = GLFWKeyCallback((long win, int key, int scancode, int action, int mods) -> {
                if(key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) glfwSetWindowShouldClose(win, GL11.GL_TRUE);
                if(key == GLFW.GLFW_KEY_N && action == GLFW_PRESS) curcolor = new float[] {(float) Math.random(),(float) Math.random(),(float) Math.random()};
        }));
        
        glfwSetMouseButtonCallback(window, mcb = GLFWMouseButtonCallback((win, button, action, mods) -> {
            if (button == GLFW.GLFW_MOUSE_BUTTON_LEFT) {
                if (action == 1) {
                    sp.LCLICK = true;
                } else {
                    sp.LCLICK = false;
                }
            } if (button == GLFW.GLFW_MOUSE_BUTTON_RIGHT) {
                if (action == 1) {
                    sp.RCLICK = true;
                    addLight(new Light(mouse(), curinten, curcolor));
                } else {
                    sp.RCLICK = false;
                }
            }
        }));
        
        glfwSetScrollCallback(window, scb = GLFWScrollCallback((win, xoffset, yoffset) -> {
            curinten += curinten*yoffset/5;
        }));
        
        glfwSetWindowSizeCallback(window, wcb = GLFWWindowSizeCallback((win, width, height) -> {
            size = new Dimension(width,height);
        }));
    }
    
    public void open() {
        drawOnWindow = true;
        draw.start();
    }
    
    public void close() {
        drawOnWindow = false;
    }
    
    
    @Override
    public void run() {
        init();
        // Make the window visible
        glfwShowWindow(window);
        long tim = System.nanoTime();
        GL11.glClearColor(0f,0f,0f,1f);
        while(drawOnWindow && glfwWindowShouldClose(window) == GL11.GL_FALSE) {
            
            GL30.glBindVertexArray(vao1);
            
            GL11.glOrtho(0, size.width, size.height, 0, 1, -1);
            GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT); // clear the framebuffer
            
            
            balls.clear();
            Draw();
            
            setBallList();
            setLightList();
            setCurrentLight(new Light(new Vector(100f,100f), curinten, curcolor));
            
            int sizeloc = GL20.glGetUniformLocation(ballProgram, "size");
            GL20.glUniform2f(sizeloc, size.width, size.height);
            
            GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE);
            
            GL20.glEnableVertexAttribArray(0);
            
            GL20.glUseProgram(lightProgram);
            GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, 6);
            
            GL20.glDisableVertexAttribArray(0);
            GL30.glBindVertexArray(vao2);
            GL20.glEnableVertexAttribArray(0);
            
            GL20.glUseProgram(ballProgram);
            GL11.glDrawArrays(GL11.GL_POINTS, 0, balls.size());
            
            // Put everything back to default (deselect)
            GL20.glDisableVertexAttribArray(0);
            GL30.glBindVertexArray(0);
            
            GL20.glUseProgram(0);
            
            glfwSwapBuffers(window); // swap the color buffers
            
            // Poll for window events. The key callback above will only be invoked during this call.
            glfwPollEvents();
        }
        dispose();
    }
    
    private List<Light> lights = new ArrayList<>();
    private List<Light> balls = new ArrayList<>();
    
    private float curcolor[] = new float[] {1f,1f,1f};
    private float curinten = 1f;
    
    public void addLight(Light l) {
        lights.add(l);
    } public void addBall(Light l) {
        balls.add(l);
    }
    
    private void setLightList() {
        for(int i = 0; i<lights.size();i++) {
            int posloc = GL20.glGetUniformLocation(lightProgram, "lights["+i+"].pos");
            int colorloc = GL20.glGetUniformLocation(lightProgram, "lights["+i+"].color");
            Light l = lights.get(i);
            GL20.glUniform3f(posloc, l.pos.x, l.pos.y,l.intensity);
            GL20.glUniform3f(colorloc, l.color[0], l.color[1], l.color[2]);
        }
    }
    
    private void setBallList() {
        float[] vertices = new float[balls.size()*3];
        for(int i = 0; i<balls.size();i++) {
            Light l = balls.get(i);
            vertices[i*3] = l.pos.x;
            vertices[i*3 + 1] = l.pos.y;
            vertices[i*3 + 2] = l.intensity;
        }
        // Sending data to OpenGL requires the usage of (flipped) byte buffers
        FloatBuffer verticesBuffer = BufferUtils.createFloatBuffer(vertices.length);
        verticesBuffer.put(vertices);
        verticesBuffer.flip();
        
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo2);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, verticesBuffer, GL15.GL_DYNAMIC_DRAW);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        
//        for(int i = 0; i<balls.size();i++) {
//            int posloc = GL20.glGetUniformLocation(shaderProgram, "balls["+i+"].pos");
//            int colorloc = GL20.glGetUniformLocation(shaderProgram, "balls["+i+"].color");
//            Light l = balls.get(i);
//            GL20.glUniform3f(posloc, l.pos.x, l.pos.y, l.intensity);
//            GL20.glUniform3f(colorloc, l.color[0], l.color[1], l.color[2]);
//        }
    }
    
    private void setCurrentLight(Light l) {
        int loc1 = GL20.glGetUniformLocation(lightProgram, "lights[0].pos");
        int loc2 = GL20.glGetUniformLocation(lightProgram, "lights[0].color");
        GL20.glUniform3f(loc1, l.pos.x, l.pos.y, l.intensity);
        GL20.glUniform3f(loc2, l.color[0], l.color[1], l.color[2]);
    }
    
    
    
    private void prepareShaders() {
        ballProgram = GL20.glCreateProgram();
        lightProgram = GL20.glCreateProgram();
        int ballvert = GL20.glCreateShader(GL20.GL_VERTEX_SHADER);
        int ballfrag = GL20.glCreateShader(GL20.GL_FRAGMENT_SHADER);
        int lightvert = GL20.glCreateShader(GL20.GL_VERTEX_SHADER);
        int lightfrag = GL20.glCreateShader(GL20.GL_FRAGMENT_SHADER);
        
        String vert = null,frag = null,vert2 = null,frag2 = null;
        
        try {
            vert = readFile("shaders/vertex.shader", StandardCharsets.UTF_8);
            frag = readFile("shaders/fragment.shader", StandardCharsets.UTF_8);
            vert2 = readFile("shaders/shader.vert", StandardCharsets.UTF_8);
            frag2 = readFile("shaders/shader.frag", StandardCharsets.UTF_8);
        } catch (IOException ex) {
            Logger.getLogger(Windu.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0);
        }
        
        GL20.glShaderSource(ballvert, vert);
        GL20.glShaderSource(ballfrag, frag);
        GL20.glShaderSource(lightvert, vert2);
        GL20.glShaderSource(lightfrag, frag2);
        GL20.glCompileShader(ballvert);
        GL20.glCompileShader(ballfrag);
        GL20.glCompileShader(lightvert);
        GL20.glCompileShader(lightfrag);
        GL20.glAttachShader(ballProgram, ballvert);
        GL20.glAttachShader(ballProgram, ballfrag);
        GL20.glAttachShader(lightProgram, lightvert);
        GL20.glAttachShader(lightProgram, lightfrag);
        GL20.glLinkProgram(ballProgram);
        GL20.glValidateProgram(ballProgram);
        GL20.glUseProgram(ballProgram);
        
        //if the link failed, throw some sort of exception
        if (GL20.glGetProgrami(ballProgram, GL20.GL_LINK_STATUS) == GL11.GL_FALSE) {
            System.err.println("Link failed");
            System.err.println(getLogInfo(ballProgram));
            System.exit(1);
        }
        
        //detach and delete the shaders which are no longer needed
        GL20.glDetachShader(ballProgram, ballvert);
        GL20.glDetachShader(ballProgram, ballfrag);
        GL20.glDeleteShader(ballvert);
        GL20.glDeleteShader(ballfrag);
        
        GL20.glUseProgram(0);
        
        GL20.glLinkProgram(lightProgram);
        GL20.glValidateProgram(lightProgram);
        GL20.glUseProgram(lightProgram);
        
        //if the link failed, throw some sort of exception
        if (GL20.glGetProgrami(lightProgram, GL20.GL_LINK_STATUS) == GL11.GL_FALSE) {
            System.err.println("Link failed");
            System.err.println(getLogInfo(lightProgram));
            System.exit(1);
        }
        
        //detach and delete the shaders which are no longer needed
        GL20.glDetachShader(lightProgram, lightvert);
        GL20.glDetachShader(lightProgram, lightfrag);
        GL20.glDeleteShader(lightvert);
        GL20.glDeleteShader(lightfrag);
        
        GL20.glUseProgram(0);
        
        
        
        // Create a new Vertex Array Object in memory and select it (bind)
        // A VAO can have up to 16 attributes (VBO's) assigned to it by default
        
        
        vao1 = GL30.glGenVertexArrays();
        GL30.glBindVertexArray(vao1);
        
        // OpenGL expects vertices to be defined counter clockwise by default
        float[] vertices = {
            // Left bottom triangle
            -1f, 1f, 0f,
            -1f, -1f, 0f,
            1f, -1f, 0f,
            // Right top triangle
            1f, -1f, 0f,
            1f, 1f, 0f,
            -1f, 1f, 0f
        };
        // Sending data to OpenGL requires the usage of (flipped) byte buffers
        FloatBuffer verticesBuffer = BufferUtils.createFloatBuffer(vertices.length);
        verticesBuffer.put(vertices);
        verticesBuffer.flip();
        
        //Create a new Vertex Buffer Object in memory and select it (bind)
        //A VBO is a collection of Vectors which in this case resemble the location of each vertex.
        vbo1 = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo1);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, verticesBuffer, GL15.GL_STATIC_DRAW);
        // Put the VBO in the attributes list at index 0
        GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 0, 0);
        // Deselect (bind to 0) the VBO
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        
        // Deselect (bind to 0) the VAO
        GL30.glBindVertexArray(0);
        
        vao2 = GL30.glGenVertexArrays();
        GL30.glBindVertexArray(vao2);
        
        // OpenGL expects vertices to be defined counter clockwise by default
        float[] verti = {};
        // Sending data to OpenGL requires the usage of (flipped) byte buffers
        FloatBuffer verticesBuffer2 = BufferUtils.createFloatBuffer(verti.length);
        verticesBuffer2.put(verti);
        verticesBuffer2.flip();
        
        //Create a new Vertex Buffer Object in memory and select it (bind)
        //A VBO is a collection of Vectors which in this case resemble the location of each vertex.
        vbo2 = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo2);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, verticesBuffer2, GL15.GL_DYNAMIC_DRAW);
        // Put the VBO in the attributes list at index 0
        GL20.glVertexAttribPointer(1, 3, GL11.GL_FLOAT, false, 0, 0);
        // Deselect (bind to 0) the VBO
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        
        // Deselect (bind to 0) the VAO
        GL30.glBindVertexArray(0);
        
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glEnable(GL32.GL_PROGRAM_POINT_SIZE);
        
        addLight(new Light(mouse(), curinten, curcolor));
    }
    
    private void updateUbo() {
        ByteBuffer uboData = BufferUtils.createByteBuffer(4*4);
        FloatBuffer fv = uboData.asFloatBuffer();
        
        /* Set viewing frustum corner rays in shader */
        fv.put(1f);
        fv.put(1f);
        fv.put(1f);
        fv.put(1f);
        fv.flip();
        
        GL15.glBindBuffer(GL31.GL_UNIFORM_BUFFER, uboid);
        GL15.glBufferSubData(GL31.GL_UNIFORM_BUFFER, 4*4, fv);
        GL15.glBindBuffer(GL31.GL_UNIFORM_BUFFER, 0);
    }
    
    public void Draw() {}
    
    public Box getBox() {
        Box box = new Box(cx,cy,size.width,size.height);
        return box;
    }
    
    public Vector mouse() {
        return new Vector(MouseInfo.getPointerInfo().getLocation().x, size.height - MouseInfo.getPointerInfo().getLocation().y);
    }
    
    private void dispose() {
        // Disable the VBO index from the VAO attributes list
        GL20.glDisableVertexAttribArray(0);
        
        // Delete the VBO
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        GL15.glDeleteBuffers(vbo1);
        
        // Delete the VAO
        GL30.glBindVertexArray(0);
        GL30.glDeleteVertexArrays(vao1);
        GLFW.glfwHideWindow(window);
        GLFW.glfwDestroyWindow(window);
        System.exit(0);
    }
    
    private static String getLogInfo(int obj) {
        return ARBShaderObjects.glGetInfoLogARB(obj, ARBShaderObjects.glGetObjectParameteriARB(obj, ARBShaderObjects.GL_OBJECT_INFO_LOG_LENGTH_ARB));
    }
    
    static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
}
