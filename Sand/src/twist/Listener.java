package twist;

import java.awt.event.*;
import java.awt.event.KeyListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import static twist.Game.sp;

public class Listener {
    
    public static class KeyListen implements KeyListener {
        private int key;
        @Override
        public void keyPressed(KeyEvent ke) {
            key = ke.getKeyCode();
            if(key == KeyEvent.VK_ESCAPE) {
                sp.close();
                sp.loopOn = false;
            }if(key == KeyEvent.VK_UP) sp.UP = true;
            if(key == KeyEvent.VK_DOWN) sp.DOWN = true;
            if(key == KeyEvent.VK_LEFT) sp.LEFT = true;
            if(key == KeyEvent.VK_RIGHT) sp.RIGHT = true;
            if(key == KeyEvent.VK_SPACE) sp.SPACE = true;
        }
        
        @Override
        public void keyReleased(KeyEvent ke) {
            key = ke.getKeyCode();
            if(key == KeyEvent.VK_UP) sp.UP = false;
            if(key == KeyEvent.VK_DOWN) sp.DOWN = false;
            if(key == KeyEvent.VK_LEFT) sp.LEFT = false;
            if(key == KeyEvent.VK_RIGHT) sp.RIGHT = false;
            if(key == KeyEvent.VK_SPACE) sp.SPACE = false;
        }
        
        @Override
        public void keyTyped(KeyEvent ke) {
            
        }
    }
    
    public static class MouseWheelListen implements MouseWheelListener {
        private double key;
        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            key = e.getWheelRotation();
            
        }
    }
    
    public static class MouseListen implements MouseListener {
        @Override
        public void mouseClicked(MouseEvent e) {
        }
        
        @Override
        public void mousePressed(MouseEvent e) {
            int key = e.getButton();
            if(key == 1) sp.LCLICK = true;
            if(key == 3) sp.RCLICK = true;
        }
        
        @Override
        public void mouseReleased(MouseEvent e) {
            int key = e.getButton();
            if(key == 1) sp.LCLICK = false;
            if(key == 3) sp.RCLICK = false;
        }
        
        @Override
        public void mouseEntered(MouseEvent e) {
        }
        
        @Override
        public void mouseExited(MouseEvent e) {
        }
    }
}
