package twist;

import phy.Vector;

public class Light {
    Vector pos;
    float intensity;
    float[] color;

    public Light(Vector pos, float intensity, float[] color) {
        this.pos = pos;
        this.intensity = intensity;
        this.color = color;
    }
}
