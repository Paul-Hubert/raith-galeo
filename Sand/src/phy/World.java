package phy;

import java.util.ArrayList;
import twist.Windu;

public class World implements java.io.Serializable {
    public ArrayList<Particule> terrain = new ArrayList<>();
    public Box b;
    public static Vector g = new Vector(0, -200);
    public static float El = 0.5f,Ef = 0.5f;
    public static boolean gravity = true;
    
    public World(Box box) {
        b = box;
    }
    
    public void work(float ms) {
        Particule p,p2;
        int ts = terrain.size();
        
        for(int i = 0; i<ts; i++) {
            p = terrain.get(i);
            if(p != null) {
                p.collide(this);
                for(int j = i + 1; j<ts; j++) {
                    if(terrain.get(j) != null) {
                        p2 = terrain.get(j);
                        //Particule.attract(p,p2,ms);
                        Particule.collide(p,p2);
                    }
                }
            }
        }
        
        
        for(int i = 0; i<ts; i++) {
            p = terrain.get(i);
            if(gravity) p.v.accel(ms,g);
            p.move(ms);
        }
    }
    
    public void draw(Windu win) {
        //g.fillRect((int) (win.w / 2 + b.x - win.x), (int) (win.h / 2 + b.y - win.y), b.w, b.h);
        
        for(int i = 0; i < terrain.size(); i++) {
            terrain.get(i).draw(win);
        }
    }
    
    public long getTotalEnergy() {
        long sum = 0;
        for(int i = 0; i<terrain.size();i++) {
            Particule p = terrain.get(i);
            float l = p.v.length();
            sum += p.m*l*l/2;
        }
        return sum;
    }
    
    
    
    Particule grabbed = null;
    
    public void grab(Vector mp) {
        if(grabbed == null) {
            for(int i = 0;i<terrain.size();i++) {
                Particule p = terrain.get(i);
                if(Vector.dist2(new Vector(mp), p.p)<p.r*p.r) {
                    grabbed = p;
                }
            }
        } else {
            Vector normal = new Vector(mp.x - grabbed.p.x, mp.y - grabbed.p.y);
            grabbed.v.add(normal.mult(1));
            grabbed = null;
        }
    }
}
