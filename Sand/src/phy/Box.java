package phy;

import java.awt.Graphics2D;

public class Box extends Vector {
    
    public int w, h;
    
    public Box(float X, float Y, int W, int H) {
        super(X,Y);
        w=W;h=H;
    }
    
    public void draw(Graphics2D g, Box win) {
        g.fillRect((int) (win.w / 2 + x - win.x), (int) (win.h / 2 + y - win.y), (int) w, (int) h);
    }
}
