package lib;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Vector implements java.io.Serializable,Cloneable {
    
    public float x, y;

    public Vector() {
        x = 0f;
        y = 0f;
    }

    public Vector(float Vx, float Vy) {
        x = Vx;
        y = Vy;
    }
    
    public Vector(Vector b) {
        x = b.x;
        y = b.y;
    }
    
    public float length() {
        return (float) Math.sqrt(x*x+y*y);
    }
    
    public float len2() {
        return  x*x+y*y;
    }
    
    public float angle() {
        return (float) (Math.atan2(y, x) - Math.PI/2);
    }
    
    void accel(float ms, Vector g) {
        x += g.x * ms / 1000;
        y += g.y * ms / 1000;
    }
    
    public static float dist(Vector a, Vector b) {
        return (float) Math.sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
    }
    
    public static float dist2(Vector a, Vector b) {
        float d2 = (a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y);
        return d2;
    }
    
    public double dot(Vector b){
        double sum = x * b.x + y * b.y;
        return sum;
    }
    
    public Vector norm() {
        float nl = length();
        return new Vector(x / nl,y / nl);
    }
    
    public Vector add(Vector b) {
        x += b.x;
        y += b.y;
        return this;
    }
    
    public Vector sub(Vector b) {
        x -= b.x;
        y -= b.y;
        return this;
    }
    
    public Vector mult(double b) {
        try {
            
            Vector v = (Vector) this.clone();
            v.x *= b;
            v.y *= b;
            return v;
            
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Vector.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public Vector div(double b) {
        try {
            Vector v = (Vector) this.clone();
            v.x /= b;
            v.y /= b;
            return v;
            
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Vector.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
