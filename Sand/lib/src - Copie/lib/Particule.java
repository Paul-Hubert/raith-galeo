package lib;

import java.awt.Color;
import java.awt.Graphics2D;
import java.io.Serializable;

public class Particule implements Serializable {
    public Vector v,p;
    float m;
    int r;

    public Particule(Vector p, int R) {
        this.p = p;
        r = R;
        v = new Vector();
        m = 0f;
    }

    public Particule(Vector p, int R, float mass) {
        this.p = p;
        r = R;
        v = new Vector();
        m = mass;
    }

    public Particule(Vector p, int R, Vector V, int mass) {
        this.p = p;
        r = R;
        v = V;
        m = mass;
    }
    
    public void move(float ms) {
        p.add(v.mult(ms/1000));
    }
        
    public static void attract(Particule a, Particule b, float ms) {
        Vector d = new Vector(a.p).sub(b.p);
        float d2 = d.len2(), G = 50f;
        
        a.v.add(d.mult(-G * b.m / d2 * ms / 1000));
        b.v.add(d.mult(G * a.m / d2 * ms / 1000));
    }
    
    void draw(Graphics2D g, Box win) {
        g.setColor(Color.LIGHT_GRAY);
        g.fillOval((int) (win.w / 2 + p.x - win.x - r), (int) (win.h / 2 + p.y - win.y - r), r * 2, r * 2);
    }
    
    void collide(World w) {
        if (p.x - r < w.b.x) {
            if(v.x < 0) v.y -= v.y * World.Ef;
            v.x = Math.abs(v.x * World.El);
            p.x = w.b.x + r;
        } else if (p.x + r > w.b.x + w.b.w) {
            if(v.x > 0) v.y -= v.y * World.Ef;
            v.x = -Math.abs(v.x * World.El);
            p.x = w.b.x + w.b.w - r;
        }
        if (p.y - r < w.b.y) {
            if(v.y < 0) v.x -= v.x * World.Ef;
            v.y = Math.abs(v.y * World.El);
            p.y = w.b.y + r;
        } else if (p.y + r > w.b.y + w.b.h) {
            if(v.y > 0) v.x -= v.x * World.Ef;
            v.y = -Math.abs(v.y * World.El);
            p.y = w.b.y + w.b.h - r;
        }
    }
    
    public static void collide(Particule a, Particule b) {
        if (Vector.dist2(a.p,b.p) < (a.r + b.r) * (a.r + b.r)) {
            
            Vector n = new Vector(a.p.x - b.p.x, a.p.y - b.p.y);
            float dist = n.length();
            n = n.div(dist);
            
            
            Vector t = new Vector(-n.y,n.x);
            double van,vat,vbn,vbt;
            van = a.v.dot(n);
            vbn = b.v.dot(n);
            vat = a.v.dot(t);
            vbt = b.v.dot(t);
            double va,vb;
            if(van > 0) {
                van -= van * World.El;
                vat -= vat * World.Ef;
            }
            if(vbn > 0) {
                vbn -= vat * World.El;
                vbt -= vbt * World.Ef;
            }
            va = (van * (a.m - b.m) + 2 * b.m * vbn) / (a.m + b.m);
            vb = (vbn * (b.m - a.m) + 2 * a.m * van) / (a.m + b.m);
            
            
            Vector av = new Vector(),bv = new Vector();
            av.add(n.mult(va)).add(t.mult(vat));
            bv.add(n.mult(vb)).add(t.mult(vbt));
            
            a.v = av;
            b.v = bv;
            
            Vector nd = n.mult((a.r + b.r - dist) / 2);
            a.p.add(nd);
            b.p.sub(nd);
            
        }
    }
}
