package twist;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.image.BufferStrategy;
import static java.lang.Thread.sleep;
import javax.swing.JFrame;
import javax.swing.JPanel;
import lib.*;
import twist.Listener.KeyListen;
import twist.Listener.MouseListen;
import twist.Listener.MouseWheelListen;

public class Windu extends JPanel implements Runnable{
    JFrame frame;
    float cx, cy;
    BufferStrategy strat;
    int w,h;
    Thread draw;
    boolean drawOnWindow;
    
    public Windu(String str, boolean full) {
        cx = 0;
        cy = 0;
        frame = new JFrame(str);
        frame.addMouseWheelListener(new MouseWheelListen());
        frame.addKeyListener(new KeyListen());
        frame.addMouseListener(new MouseListen());
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH); //fullsreen with borders
        frame.setUndecorated(full);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.getContentPane().add(this);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.createBufferStrategy(2);
        strat = frame.getBufferStrategy();
        draw = new Thread(this);
        frame.setVisible(true);
        w = frame.getWidth();
        h = frame.getHeight();
        frame.setVisible(false);
    }
    
    public void open() {
        frame.setVisible(true);
        w = frame.getWidth();
        h = frame.getHeight();
        drawOnWindow = true;
        draw.start();
    }
    
    public void close() {
        frame.setVisible(false);
        drawOnWindow = false;
    }
    
    
    @Override
    public void run() {
        while(drawOnWindow) {
            do {
                do {
                    Graphics2D graphics = (Graphics2D) strat.getDrawGraphics();
                    Draw(graphics);
                    graphics.dispose();
                } while (strat.contentsRestored());
                strat.show();
            } while (strat.contentsLost());
            
            try {
                sleep(1000/60);
            } catch (InterruptedException ex) {
                System.out.println("Thread interrupted");
            }
        }
        frame.dispose();
    }
    
    public void Draw(Graphics2D g) {
        g.setColor(Color.WHITE);
        g.fillRect(0,0,w,h);
    }
    
    public Box getBox() {
        Box box = new Box(cx,cy,w,h);
        return box;
    }
    
    public Vector mouse() {
        return new Vector(MouseInfo.getPointerInfo().getLocation().x,MouseInfo.getPointerInfo().getLocation().y);
    }
}
