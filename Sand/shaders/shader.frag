#version 430

struct Light {
    vec3 pos;
    vec3 color;
};

const int size = 80;
uniform Light lights[size];

void main() {
    //vec3 lightAttenuation = vec3(1.0,0.01,0.0001);
    
    for(int i = 0; i<size; i++) {
        Light l = lights[i];
        if(l.pos.x==0.0 && i == 0) {
            gl_FragColor += vec4(0.5,0.01,0.01,0.01);
            break;
        } else {
            gl_FragColor = vec4(1.0,1.0,1.0,1.0);
        }
        vec2 aux = l.pos.xy - gl_FragCoord.xy;
        float distance = length(aux);
        float med = (l.color.x+l.color.y+l.color.z)/3;
        float att = l.pos.z*med/distance;
        vec4 color = vec4(att,att,att,1.0)*vec4(l.color,1.0);
        gl_FragColor += color;
    }
}

//for(int i = 0; i<lsize; i++) {
//        Light l = lights[i];
//        if(l.pos.x==0.0&&l.pos.y==0.0) break;
//        vec2 temp = l.pos.xy;
//        vec2 aux = l.pos.xy - gl_FragCoord.xy;
//        float distance = length(aux);
//        aux /= distance;
//        aux *= 5;
//        bool bill = true;
//        
//        for(int j=0; j<round(distance)/5; j++) {
//            temp -= aux;
//            for(int k = 0; k<bsize; k++) {
//                Light b = balls[k];
//                if(b.pos.x==0.0&&b.pos.y==0.0) break;
//                vec2 aux2 = temp - b.pos.xy;
//                float dist2 = aux2.x*aux2.x+aux2.y*aux2.y;
//                if(dist2 < b.pos.z*b.pos.z) {
//                    bill = false;
//                    break;
//                }
//            }
//            if(!bill) {break;}
//        }
//        if(bill) {
//            float med = (l.color.x+l.color.y+l.color.z)/3;
//            float att = l.pos.z*med/distance;
//            vec4 color = vec4(att,att,att,1.0)*vec4(l.color,1.0);
//            gl_FragColor += color;
//        } 
//    }
//    
//    //for(int i = 0; i<bsize; i++) {
//    //    Light l = balls[i];
//    //    vec2 aux = l.pos.xy - gl_FragCoord.xy;
//    //	float dist2 = aux.x*aux.x+aux.y*aux.y;
//    //    if(dist2 < l.pos.z*l.pos.z) {
//    //        gl_FragColor += gl_FragColor * vec4(l.color,1.0);
//    //    }
//    //}