#version 430

in vec3 Position;
out vec3 Pos;

uniform vec2 size;

void main() {
    Pos = Position;
    vec3 p = vec3((Position.x) / (size.x / 2) , (Position.y) / (size.y / 2) , 0.0);
    gl_Position = vec4(p, 1.0);
    gl_PointSize = Position.z * 2;
}
