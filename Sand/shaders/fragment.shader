#version 430

in vec3 Pos;

uniform vec2 size;

void main() {
    vec3 p = vec3(Pos.x + size.x/2, Pos.y + size.y/2, Pos.z);
    vec2 aux = gl_FragCoord.xy - p.xy;
    float dist2 = aux.x*aux.x+aux.y*aux.y;
    if(dist2 < p.z*p.z) {
        gl_FragColor = vec4(1.0,1.0,1.0,1.0);
    }
    gl_FragColor = vec4(1.0,1.0,1.0,1.0);
}
