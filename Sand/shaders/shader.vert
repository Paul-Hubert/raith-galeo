#version 430

struct Light {
    vec3 pos;
    vec3 color;
};

const int size = 80;
uniform Light lights[size];

in vec3 Position;

void main() {
    gl_Position = vec4(Position, 1.0);
}
